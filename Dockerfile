########## snow-build
FROM --platform=linux/amd64 debian:bullseye-slim AS snow-build
WORKDIR /work
ADD https://sfc-repo.snowflakecomputing.com/snowsql/bootstrap/1.2/linux_x86_64/snowsql-1.2.21-linux_x86_64.bash snowsql.bash
ADD https://sfc-repo.snowflakecomputing.com/odbc/linux/2.24.7/snowflake_linux_x8664_odbc-2.24.7.tgz snowflake_linux_x8664_odbc.tgz
COPY snowflake/conf ./
ENV WORKSPACE /var/snowsql
ENV LC_ALL=C.UTF-8 LANG=C.UTF-8
# Install prereqs.
ARG sf_account
RUN apt-get -qq update \
    && apt-get -qq --no-install-recommends install odbcinst \
    && gunzip -f *.tgz && tar xf *.tar  \
    && mkdir odbc \
    && mv snowflake_odbc/lib snowflake_odbc/ErrorMessages odbc/ \
    && mv simba.snowflake.ini odbc/lib/ \
    && perl -i -pe "s/SF_ACCOUNT/$sf_account/g" odbc.ini \
    && sed -e '1,/^exit$/d' snowsql.bash | tar zxf - \
    && ./snowsql -Uv \
    && echo "[connections]\naccountname = $sf_account\n\n[options]\nnoup = true" > /var/snowsql/.snowsql/config

########## exa-build
FROM  --platform=linux/amd64 debian:bullseye-slim AS exa-build
WORKDIR /work
# Download and unpack EXAplus and the ODBC driver.
ADD https://x-up.s3.amazonaws.com/7.x/7.1.17/EXAplus-7.1.17.tar.gz ./
ADD https://x-up.s3.amazonaws.com/7.x/7.1.17/EXASOL_ODBC-7.1.17.tar.gz ./
RUN tar zxf EXASOL_ODBC-*.tar.gz \
    && mv EXASOL_ODBC-*/lib/linux/x86_64/ odbc \
    && tar zxf EXAplus-*.tar.gz \
    && mv EXAplus-*/ exaplus \
    && rm -rf exaplus/doc

########## main sqitch
FROM --platform=linux/amd64 sqitch/sqitch:v1.3.0.0
USER root
RUN apt-get -qq update \
    && mkdir -p /usr/share/man/man1 \
    && apt-get -qq --no-install-recommends install unixodbc openjdk-11-jdk-headless jq curl procps \
    && apt-get clean \
    && rm -rf /var/cache/apt/* /var/lib/apt/lists/* \
    && rm -rf /man /usr/share/man /usr/share/doc \
    && mkdir -p /opt/exasol/ \
    && ln -s /opt/exaplus/exaplus /bin
COPY --from=exa-build /work/exaplus /opt/exaplus/
COPY --from=exa-build /work/odbc /opt/exasol/odbc/lib
COPY --from=snow-build /work/snowsql /bin/
COPY --from=snow-build --chown=sqitch:sqitch /var/snowsql /var/
COPY --from=snow-build /work/odbc /usr/lib/snowflake/odbc/
COPY snowflake /repo/snowflake/
COPY exasol /repo/exasol/
COPY odbcinst.ini /etc/
USER sqitch
ENV SNOWSQL_DOWNLOAD_DIR /var/.snowsql
