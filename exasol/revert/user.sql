-- Revert test:01_user from exasol

-- XXX Add DDLs here.
DROP USER AUTHENTICATOR CASCADE;
DROP USER dvb_user CASCADE;
DROP USER dvb_user_readonly CASCADE;
DROP USER dvb_admin CASCADE;
DROP USER dvb_operations CASCADE;
DROP USER dbadmin CASCADE;
DROP USER pgagent CASCADE;

COMMIT;
