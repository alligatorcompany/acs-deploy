-- Deploy dvb:update_6130 to exasol

set define off;
set escape off;
ALTER TABLE DVB_LOG.JOB_LOAD_LOG ADD COLUMN IF NOT EXISTS (UNIQUE_JOB_RUN_ID INTEGER);
ALTER TABLE DVB_LOG.DATAVAULT_LOAD_LOG ADD COLUMN IF NOT EXISTS (UNIQUE_JOB_RUN_ID INTEGER);
ALTER TABLE DVB_LOG.STAGING_LOAD_LOG ADD COLUMN IF NOT EXISTS (UNIQUE_JOB_RUN_ID INTEGER);
ALTER TABLE DVB_LOG.STAGING_LOAD_LOG ADD COLUMN IF NOT EXISTS (IS_DELTA_LOAD BOOLEAN DEFAULT FALSE);
ALTER TABLE DVB_LOG.STAGING_LOAD_LOG ADD COLUMN IF NOT EXISTS (EXECUTED_SOURCE_QUERY VARCHAR(2000000) UTF8);
ALTER TABLE DVB_LOG.STAGING_LOAD_LOG ADD COLUMN IF NOT EXISTS (QUERY_PARAMETERS VARCHAR(2000000) UTF8);
ALTER TABLE DVB_LOG.STAGING_LOAD_LOG ADD COLUMN IF NOT EXISTS (CUSTOM_SOURCE_QUERY_TEMPLATE VARCHAR(2000000) UTF8);
ALTER TABLE STAGING."_DVB_RUNTIME_TABLE_STATUS" ADD COLUMN IF NOT EXISTS (EXECUTED_SOURCE_QUERY VARCHAR(2000000) UTF8);
ALTER TABLE STAGING."_DVB_RUNTIME_TABLE_STATUS" ADD COLUMN IF NOT EXISTS (QUERY_PARAMETERS VARCHAR(2000000) UTF8);

CREATE OR REPLACE VIEW "DVB_CORE"."LATEST_DATAVAULT_LOAD_INFO" AS 
  SELECT
  dlog.load_entry_time,
  dlog.object_id,
  dlog.staging_table_id,
  dlog.load_start_time,
  dlog.load_end_time,
  CASE
    WHEN LOWER(dlog.load_state) = 'succeeded' OR
      LOWER(dlog.load_state) = 'failed' THEN dvb_core.f_get_time_interval_string(dlog.load_start_time, dlog.load_end_time)
    WHEN LOWER(dlog.load_state) = 'loading' THEN dvb_core.f_get_time_interval_string(dlog.load_start_time, LOCALTIMESTAMP)
    ELSE '0s'
  END AS load_duration,
  dlog.load_state,
  dlog.load_result,
  CASE
    WHEN dlog.load_progress >= 0 THEN dlog.load_progress
    ELSE NULL
  END AS load_progress,
  CASE
    WHEN dlog.load_total_rows >= 0 THEN dlog.load_total_rows
    ELSE NULL
  END AS load_total_rows,
  dlog.login_username,
  dlog.pg_username,
  dlog.job_id,
  dlog.unique_job_run_id,
  dlog.pid
FROM  (SELECT
  dll.load_entry_id,
  dll.load_entry_time,
  dll.object_id,
  dll.staging_table_id,
  dll.load_start_time,
  dll.load_end_time,
  dll.load_state,
  dll.load_result,
  dll.load_progress,
  dll.load_total_rows,
  dll.login_username,
  dll.pg_username,
  dll.pid,
  dll.job_id,
  dll.unique_job_run_id,
  row_number() OVER(PARTITION BY dll.object_id, dll.staging_table_id ORDER BY dll.load_entry_id DESC) AS load_log_rank
FROM dvb_log.datavault_load_log dll
WHERE dll.load_state IS NOT NULL
) dlog
  WHERE load_log_rank = 1
;

CREATE OR REPLACE VIEW "DVB_CORE"."LATEST_JOB_LOAD_INFO" AS 
SELECT
  jlog.load_entry_time,
  jlog.job_id,
  jlog.load_start_time AS latest_load_start_time,
  jlog.load_end_time AS latest_load_end_time,
  jsuc.load_start_time AS succeeded_load_start_time,
  jsuc.load_end_time AS succeeded_load_end_time,
  jfail.load_start_time AS failed_load_start_time,
  dvb_core.f_get_time_interval_string(jsuc.load_start_time, jsuc.load_end_time) AS succeeded_load_duration,
  CASE
    WHEN LOWER(jlog.load_state) = 'loading' THEN dvb_core.f_get_time_interval_string(jlog.load_start_time, LOCALTIMESTAMP ) 
    ELSE '0s'
  END AS current_loading_duration,
  jlog.load_state,
  jlog.load_result,
  ((COALESCE(jlog.login_username || ' ', '') || '(') || jlog.pg_username) || ')' AS
  username,
  jlog.where_clause_parameters,
  jlog.is_delta_load,
  jlog.unique_job_run_id,
  jlog.pid
FROM (
  SELECT
  jll.load_entry_id,
  jll.load_entry_time,
  jll.job_id,
  jll.load_start_time,
  jll.load_end_time,
  jll.load_state,
  jll.load_result,
  jll.login_username,
  jll.pg_username,
  jll.pid,
  jll.where_clause_parameters,
  jll.is_delta_load,
  jll.unique_job_run_id,
  ROW_NUMBER() OVER (PARTITION BY jll.job_id ORDER BY jll.load_entry_id DESC) AS load_log_rank
FROM dvb_log.job_load_log jll
WHERE jll.load_state IS NOT NULL

) jlog
LEFT JOIN (
  SELECT
  jll2.job_id,
  jll2.load_start_time,
  jll2.load_end_time,
  ROW_NUMBER() OVER (PARTITION BY jll2.job_id ORDER BY jll2.load_entry_id DESC) AS load_log_rank
FROM dvb_log.job_load_log jll2
WHERE LOWER(jll2.load_state) = 'succeeded') jsuc
  ON jsuc.job_id = jlog.job_id
  AND jsuc.load_log_rank = 1
LEFT JOIN (SELECT
  jll3.job_id,
  jll3.load_start_time,
  jll3.load_end_time,
  ROW_NUMBER() OVER (PARTITION BY jll3.job_id ORDER BY jll3.load_entry_id DESC) AS load_log_rank
FROM dvb_log.job_load_log jll3
WHERE LOWER(jll3.load_state) = 'failed') jfail
  ON jfail.job_id = jlog.job_id
  AND jfail.load_log_rank = 1
WHERE jlog.load_log_rank = 1
;

CREATE OR REPLACE VIEW "DVB_CORE"."LATEST_STAGING_LOAD_INFO" AS 
  SELECT
  slog.load_entry_time,
  slog.staging_table_id,
  slog.source_table_id,
  slog.load_start_time AS latest_load_start_time,
  slog.load_end_time AS latest_load_end_time,
  ssuc.load_start_time AS succeeded_load_start_time,
  ssuc.load_end_time AS succeeded_load_end_time,
  sfail.load_start_time AS failed_load_start_time,
  dvb_core.f_get_time_interval_string(ssuc.LOAD_START_TIME, ssuc.LOAD_END_TIME) AS succeeded_load_duration,
  CASE
    WHEN (LOWER(slog.load_state) = 'loading') THEN dvb_core.f_get_time_interval_string(slog.load_start_time, LOCALTIMESTAMP)
  ELSE NULL
  END AS current_loading_duration,
  slog.load_state,
  slog.load_result,
  slog.load_progress,
  slog.load_total_rows,
  CAST(ROUND(
    CASE 
      WHEN (slog.load_progress_percent_unlimited  < 0 OR slog.load_progress_percent_unlimited > 100) THEN NULL
      ELSE slog.load_progress_percent_unlimited
    END, 0) AS DECIMAL(5,2)) AS load_progress_percent,
  COALESCE(slog.login_username || ' ', '') || '(' || slog.pg_username || ')' AS username,
  slog.from_system_load,
  slog.job_id,
  slog.unique_job_run_id,
  slog.is_delta_load,
  slog.executed_source_query,
  slog.query_parameters,
  slog.custom_source_query_template,
  slog.pid
FROM (
  SELECT sll.load_entry_id,
    sll.load_entry_time,
    sll.staging_table_id,
    sll.source_table_id,
    sll.load_start_time,
    sll.load_end_time,
    sll.load_state,
    sll.load_result,
    sll.load_progress,
    sll.load_total_rows,
    ((100 * sll.load_progress) / ((sll.load_total_rows) + 0.000000001)) AS load_progress_percent_unlimited,
    sll.login_username,
    sll.pg_username,
    sll.from_system_load,
    sll.job_id,
    sll.unique_job_run_id,
    sll.is_delta_load,
    sll.executed_source_query,
    sll.query_parameters,
    sll.custom_source_query_template,
    sll.pid,
    row_number() OVER(PARTITION BY sll.staging_table_id ORDER BY sll.load_entry_id DESC) AS load_log_rank
  FROM dvb_log.staging_load_log sll
  WHERE sll.load_state IS NOT NULL
  ) slog
  LEFT JOIN
    (SELECT sll2.staging_table_id,
      sll2.load_start_time,
      sll2.load_end_time,
      row_number() OVER(PARTITION BY sll2.staging_table_id ORDER BY sll2.load_entry_id DESC) AS load_log_rank
    FROM dvb_log.staging_load_log sll2
    WHERE lower(sll2.load_state) = 'succeeded') ssuc
    ON ssuc.staging_table_id = slog.staging_table_id AND ssuc.load_log_rank = 1
  LEFT JOIN
    (SELECT sll3.staging_table_id,
      sll3.load_start_time,
      sll3.load_end_time,
      row_number() OVER(PARTITION BY sll3.staging_table_id ORDER BY sll3.load_entry_id DESC) AS load_log_rank
    FROM dvb_log.staging_load_log sll3
    WHERE lower(sll3.load_state) = 'failed') sfail
    ON sfail.staging_table_id = slog.staging_table_id AND sfail.load_log_rank = 1
  WHERE slog.load_log_rank = 1;
  
CREATE OR REPLACE VIEW "DVB_CORE"."X_LATEST_LOAD_INFO"
AS
  SELECT combined.entry_time,
         combined.source_id,
         combined.target_id,
         combined.start_time,
         combined.end_time,
         combined.duration,
         combined."STATE",
         combined."RESULT",
         combined.progress,
         combined.total_rows,
         combined.username,
         combined.job_id,
         combined.unique_job_run_id,
         combined.pid
  FROM (
         SELECT lsli.load_entry_time AS entry_time,
                dvb_core.f_string_between(lsli.staging_table_id, '.', '_R_') AS source_id,
                lsli.staging_table_id AS target_id,
                lsli.latest_load_start_time AS start_time,
                lsli.latest_load_end_time AS end_time,
                COALESCE(lsli.current_loading_duration, lsli.succeeded_load_duration) AS duration,
                lsli.load_state AS "STATE",
                lsli.load_result AS "RESULT",
                lsli.load_progress AS progress,
                lsli.load_total_rows AS total_rows,
                lsli.username,
                lsli.job_id,
                lsli.unique_job_run_id,
                lsli.pid
         FROM dvb_core.latest_staging_load_info lsli
         UNION ALL
         SELECT ldli.load_entry_time AS entry_time,
                ldli.staging_table_id AS source_id,
                'DATAVAULT.' || ldli.object_id AS target_id,
                ldli.load_start_time AS start_time,
                ldli.load_end_time AS end_time,
                ldli.load_duration AS duration,
                ldli.load_state AS "STATE",
                ldli.load_result AS "RESULT",
                ldli.load_progress AS progress,
                ldli.load_total_rows AS total_rows,
                ldli.login_username AS username,
                ldli.job_id,
                ldli.unique_job_run_id,
                ldli.pid
         FROM dvb_core.latest_datavault_load_info ldli
       ) combined
  ORDER BY combined.entry_time;
  
CREATE OR REPLACE VIEW "DVB_CORE"."LOAD_LOG_DATAVAULT" AS
  SELECT llp.load_entry_id,
    llp.load_entry_time,
    llp.object_id,
    llp.staging_table_id,
    llp.load_start_time,
    llp.load_end_time,
    dvb_core.f_get_time_interval_string(llp.load_start_time, llp.load_end_time) AS duration,
    llp.load_state,
    llp.load_result,
    llp.load_total_rows,
    llp.login_username,
    llp.load_progress,
    llp.job_id,
    llp.unique_job_run_id,
    llp.pg_username
  FROM ( SELECT lls.load_entry_id,
            lls.load_entry_time,
            lls.object_id,
            lls.staging_table_id,
            lls.load_start_time,
            lls.load_end_time,
            lls.load_state,
            lls.load_result,
            lls.login_username,
            lls.pg_username,
            lls.load_progress,
            lls.load_total_rows,
            lls.job_id,
            lls.unique_job_run_id,
            row_number() OVER (PARTITION BY lls.object_id, lls.staging_table_id, lls.load_entry_time ORDER BY lls.load_entry_id DESC) AS entry_order
           FROM ( SELECT ll.load_entry_id,
                    ll.load_entry_time,
                    ll.object_id,
                    ll.staging_table_id,
                    ll.load_start_time,
                    ll.load_end_time,
                    ll.load_state,
                    ll.load_result,
                    ll.login_username,
                    ll.pg_username,
                    ll.load_progress,
                    ll.load_total_rows,
                    ll.job_id,
                    ll.unique_job_run_id
                   FROM dvb_log.datavault_load_log ll
                  WHERE (ll.load_state IS NOT NULL)
                 ) lls) llp
  WHERE (llp.entry_order = 1);
  
CREATE OR REPLACE VIEW "DVB_CORE"."LOAD_LOG_JOB" AS
  SELECT llp.load_entry_id,
    llp.load_entry_time,
    llp.job_id,
    llp.load_start_time,
    llp.load_end_time,
    dvb_core.f_get_time_interval_string(llp.load_start_time, llp.load_end_time) AS duration,
    llp.load_state,
    llp.load_result,
    llp.login_username,
    llp.pg_username,
    llp.pid,
    llp.where_clause_parameters,
    llp.is_delta_load,
    llp.unique_job_run_id
  FROM ( SELECT lls.load_entry_id,
            lls.load_entry_time,
            lls.job_id,
            lls.load_start_time,
            lls.load_end_time,
            lls.load_state,
            lls.load_result,
            lls.login_username,
            lls.pg_username,
            lls.pid,
            lls.where_clause_parameters,
            lls.is_delta_load,
            lls.unique_job_run_id,
            row_number() OVER (PARTITION BY lls.job_id, lls.load_entry_time ORDER BY lls.load_entry_id DESC) AS entry_order
           FROM ( SELECT ll.load_entry_id,
                    ll.load_entry_time,
                    ll.job_id,
                    ll.load_start_time,
                    ll.load_end_time,
                    ll.load_state,
                    ll.load_result,
                    ll.login_username,
                    ll.pg_username,
                    ll.pid,
                    ll.where_clause_parameters,
                    ll.is_delta_load,
                    ll.unique_job_run_id
                   FROM dvb_log.job_load_log ll
                  WHERE (ll.load_state IS NOT NULL)
                 ) lls) llp
  WHERE (llp.entry_order = 1);
  
CREATE OR REPLACE VIEW "DVB_CORE"."LOAD_LOG_STAGING" AS
  SELECT llp.load_entry_id,
    llp.load_entry_time,
    llp.source_table_id,
    llp.staging_table_id,
    llp.load_start_time,
    llp.load_end_time,
    dvb_core.f_get_time_interval_string(llp.load_start_time, llp.load_end_time) AS duration,
    llp.load_progress,
    llp.load_total_rows,
    llp.load_state,
    llp.load_result,
    llp.login_username,
    llp.pg_username,
    llp.job_id,
    llp.unique_job_run_id,
    llp.is_delta_load,
    llp.executed_source_query,
    llp.query_parameters,
    llp.custom_source_query_template
  FROM ( SELECT lls.load_entry_id,
            lls.load_entry_time,
            lls.source_table_id,
            lls.staging_table_id,
            lls.load_start_time,
            lls.load_end_time,
            lls.load_progress,
            lls.load_total_rows,
            lls.load_state,
            lls.load_result,
            lls.login_username,
            lls.pg_username,
            lls.job_id,
            lls.unique_job_run_id,
            lls.is_delta_load,
            lls.executed_source_query,
            lls.query_parameters,
            lls.custom_source_query_template,
            row_number() OVER (PARTITION BY lls.staging_table_id, lls.load_entry_time ORDER BY lls.load_entry_id DESC) AS entry_order
           FROM ( SELECT ll.load_entry_id,
                    ll.load_entry_time,
                    ll.source_table_id,
                    ll.staging_table_id,
                    ll.load_start_time,
                    ll.load_end_time,
                    ll.load_progress,
                    ll.load_total_rows,
                    ll.load_state,
                    ll.load_result,
                    ll.login_username,
                    ll.pg_username,
                    ll.job_id,
                    ll.unique_job_run_id,
                    ll.is_delta_load,
                    ll.executed_source_query,
                    ll.query_parameters,
                    ll.custom_source_query_template
                   FROM dvb_log.staging_load_log ll
                  WHERE (ll.load_state IS NOT NULL)
                 ) lls) llp
  WHERE (llp.entry_order = 1);

COMMIT;
