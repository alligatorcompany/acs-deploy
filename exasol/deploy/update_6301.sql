-- Deploy dvb:update_6301 to exasol

set define off;
set escape off;
ALTER TABLE STAGING."_DVB_RUNTIME_TABLE_STATUS"
  ADD COLUMN IF NOT EXISTS ("IS_INITIAL_LOAD" BOOLEAN DEFAULT FALSE);
  
ALTER TABLE STAGING."_DVB_RUNTIME_TABLE_STATUS"
  ADD COLUMN IF NOT EXISTS ("IS_CDC_LOAD" BOOLEAN DEFAULT FALSE);

ALTER TABLE DVB_LOG.STAGING_LOAD_LOG
  ADD COLUMN IF NOT EXISTS ("IS_INITIAL_LOAD" BOOLEAN DEFAULT FALSE);
  
ALTER TABLE DVB_LOG.STAGING_LOAD_LOG
  ADD COLUMN IF NOT EXISTS ("IS_CDC_LOAD" BOOLEAN DEFAULT FALSE);
    
ALTER TABLE DVB_LOG.JOB_LOAD_LOG
  ADD COLUMN IF NOT EXISTS ("IS_INITIAL_LOAD" BOOLEAN DEFAULT FALSE);
  
ALTER TABLE DVB_LOG.JOB_LOAD_LOG
  ADD COLUMN IF NOT EXISTS ("IS_CDC_LOAD" BOOLEAN DEFAULT FALSE);
  
CREATE OR REPLACE VIEW "DVB_CORE"."LATEST_STAGING_LOAD_INFO" AS 
  SELECT
  slog.load_entry_time,
  slog.staging_table_id,
  slog.source_table_id,
  slog.load_start_time AS latest_load_start_time,
  slog.load_end_time AS latest_load_end_time,
  ssuc.load_start_time AS succeeded_load_start_time,
  ssuc.load_end_time AS succeeded_load_end_time,
  sfail.load_start_time AS failed_load_start_time,
  dvb_core.f_get_time_interval_string(ssuc.LOAD_START_TIME, ssuc.LOAD_END_TIME) AS succeeded_load_duration,
  CASE
    WHEN (LOWER(slog.load_state) = 'loading') THEN dvb_core.f_get_time_interval_string(slog.load_start_time, LOCALTIMESTAMP)
  ELSE NULL
  END AS current_loading_duration,
  slog.load_state,
  slog.load_result,
  slog.load_progress,
  slog.load_total_rows,
  CAST(ROUND(
    CASE 
      WHEN (slog.load_progress_percent_unlimited  < 0 OR slog.load_progress_percent_unlimited > 100) THEN NULL
      ELSE slog.load_progress_percent_unlimited
    END, 0) AS DECIMAL(5,2)) AS load_progress_percent,
  COALESCE(slog.login_username || ' ', '') || '(' || slog.pg_username || ')' AS username,
  slog.from_system_load,
  slog.job_id,
  slog.unique_job_run_id,
  slog.is_delta_load,
  slog.is_cdc_load,
  slog.is_initial_load,
  slog.executed_source_query,
  slog.query_parameters,
  slog.custom_source_query_template,
  slog.pid
FROM (
  SELECT sll.load_entry_id,
    sll.load_entry_time,
    sll.staging_table_id,
    sll.source_table_id,
    sll.load_start_time,
    sll.load_end_time,
    sll.load_state,
    sll.load_result,
    sll.load_progress,
    sll.load_total_rows,
    ((100 * sll.load_progress) / ((sll.load_total_rows) + 0.000000001)) AS load_progress_percent_unlimited,
    sll.login_username,
    sll.pg_username,
    sll.from_system_load,
    sll.job_id,
    sll.unique_job_run_id,
    sll.is_delta_load,
	sll.is_cdc_load,
	sll.is_initial_load,
    sll.executed_source_query,
    sll.query_parameters,
    sll.custom_source_query_template,
    sll.pid,
    row_number() OVER(PARTITION BY sll.staging_table_id ORDER BY sll.load_entry_id DESC) AS load_log_rank
  FROM dvb_log.staging_load_log sll
  WHERE sll.load_state IS NOT NULL
  ) slog
  LEFT JOIN
    (SELECT sll2.staging_table_id,
      sll2.load_start_time,
      sll2.load_end_time,
      row_number() OVER(PARTITION BY sll2.staging_table_id ORDER BY sll2.load_entry_id DESC) AS load_log_rank
    FROM dvb_log.staging_load_log sll2
    WHERE lower(sll2.load_state) = 'succeeded') ssuc
    ON ssuc.staging_table_id = slog.staging_table_id AND ssuc.load_log_rank = 1
  LEFT JOIN
    (SELECT sll3.staging_table_id,
      sll3.load_start_time,
      sll3.load_end_time,
      row_number() OVER(PARTITION BY sll3.staging_table_id ORDER BY sll3.load_entry_id DESC) AS load_log_rank
    FROM dvb_log.staging_load_log sll3
    WHERE lower(sll3.load_state) = 'failed') sfail
    ON sfail.staging_table_id = slog.staging_table_id AND sfail.load_log_rank = 1
  WHERE slog.load_log_rank = 1;
  
CREATE OR REPLACE VIEW "DVB_CORE"."LATEST_JOB_LOAD_INFO" AS 
SELECT
  jlog.load_entry_time,
  jlog.job_id,
  jlog.load_start_time AS latest_load_start_time,
  jlog.load_end_time AS latest_load_end_time,
  jsuc.load_start_time AS succeeded_load_start_time,
  jsuc.load_end_time AS succeeded_load_end_time,
  jfail.load_start_time AS failed_load_start_time,
  dvb_core.f_get_time_interval_string(jsuc.load_start_time, jsuc.load_end_time) AS succeeded_load_duration,
  CASE
    WHEN LOWER(jlog.load_state) = 'loading' THEN dvb_core.f_get_time_interval_string(jlog.load_start_time, LOCALTIMESTAMP ) 
    ELSE '0s'
  END AS current_loading_duration,
  jlog.load_state,
  jlog.load_result,
  ((COALESCE(jlog.login_username || ' ', '') || '(') || jlog.pg_username) || ')' AS
  username,
  jlog.where_clause_parameters,
  jlog.is_delta_load,
  jlog.is_cdc_load,
  jlog.is_initial_load,
  jlog.unique_job_run_id,
  jlog.pid
FROM (
  SELECT
  jll.load_entry_id,
  jll.load_entry_time,
  jll.job_id,
  jll.load_start_time,
  jll.load_end_time,
  jll.load_state,
  jll.load_result,
  jll.login_username,
  jll.pg_username,
  jll.pid,
  jll.where_clause_parameters,
  jll.is_delta_load,
  jll.is_cdc_load,
  jll.is_initial_load,
  jll.unique_job_run_id,
  ROW_NUMBER() OVER (PARTITION BY jll.job_id ORDER BY jll.load_entry_id DESC) AS load_log_rank
FROM dvb_log.job_load_log jll
WHERE jll.load_state IS NOT NULL

) jlog
LEFT JOIN (
  SELECT
  jll2.job_id,
  jll2.load_start_time,
  jll2.load_end_time,
  ROW_NUMBER() OVER (PARTITION BY jll2.job_id ORDER BY jll2.load_entry_id DESC) AS load_log_rank
FROM dvb_log.job_load_log jll2
WHERE LOWER(jll2.load_state) = 'succeeded') jsuc
  ON jsuc.job_id = jlog.job_id
  AND jsuc.load_log_rank = 1
LEFT JOIN (SELECT
  jll3.job_id,
  jll3.load_start_time,
  jll3.load_end_time,
  ROW_NUMBER() OVER (PARTITION BY jll3.job_id ORDER BY jll3.load_entry_id DESC) AS load_log_rank
FROM dvb_log.job_load_log jll3
WHERE LOWER(jll3.load_state) = 'failed') jfail
  ON jfail.job_id = jlog.job_id
  AND jfail.load_log_rank = 1
WHERE jlog.load_log_rank = 1
;

COMMIT;
