-- Deploy test:01_user to exasol
-- requires: 00_config_pre

-- XXX Add DDLs here.

CREATE USER authenticator
  IDENTIFIED BY "doNotLogin!"
  ;

GRANT CREATE SESSION TO authenticator;

CREATE USER dvb_user IDENTIFIED BY KERBEROS PRINCIPAL 'dvb_user@does_not_exists';
CREATE USER dvb_user_readonly IDENTIFIED BY KERBEROS PRINCIPAL 'dvb_user_readonly@does_not_exists';
CREATE USER dvb_admin IDENTIFIED BY KERBEROS PRINCIPAL 'dvb_admin@does_not_exists';
CREATE USER dvb_operations IDENTIFIED BY KERBEROS PRINCIPAL 'dvb_operations@does_not_exists';
CREATE USER dbadmin IDENTIFIED BY KERBEROS PRINCIPAL 'dbadmin@does_not_exists';
CREATE USER pgagent IDENTIFIED BY KERBEROS PRINCIPAL 'pgagent@does_not_exists';

-- restrict as needed...

GRANT ALL PRIVILEGES ON SCHEMA DVB_CONFIG TO dvb_admin, dbadmin;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE DVB_CONFIG.JOB_DATA TO dvb_user, dvb_operations, dvb_admin, dbadmin, pgagent;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE DVB_CONFIG.JOB_LOADS TO dvb_user, dvb_operations, dvb_admin, dbadmin, pgagent;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE DVB_CONFIG.JOB_SCHEDULES TO dvb_user, dvb_operations, dvb_admin, dbadmin, pgagent;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE DVB_CONFIG.JOB_SQL_QUERIES TO dvb_user, dvb_operations, dvb_admin, dbadmin, pgagent;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE DVB_CONFIG.JOB_TRIGGERS TO dvb_user, dvb_operations, dvb_admin, dbadmin, pgagent;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE DVB_CONFIG.SYSTEM_COLORS TO dvb_user, dvb_operations, dvb_admin, dbadmin, pgagent;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE DVB_CONFIG.SYSTEM_DATA TO dvb_user, dvb_operations, dvb_admin, dbadmin, pgagent;
 
GRANT SELECT ON TABLE DVB_CONFIG.JOB_DATA TO dvb_user_readonly;
GRANT SELECT ON TABLE DVB_CONFIG.JOB_LOADS TO dvb_user_readonly;
GRANT SELECT ON TABLE DVB_CONFIG.JOB_SCHEDULES TO dvb_user_readonly;
GRANT SELECT ON TABLE DVB_CONFIG.JOB_SQL_QUERIES TO dvb_user_readonly;
GRANT SELECT ON TABLE DVB_CONFIG.JOB_TRIGGERS TO dvb_user_readonly;
GRANT SELECT ON TABLE DVB_CONFIG.SYSTEM_COLORS TO dvb_user_readonly;
GRANT SELECT ON TABLE DVB_CONFIG.SYSTEM_DATA TO dvb_user_readonly;
  
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE DVB_CONFIG."_DVB_RUNTIME_BOOKMARK" TO dvb_user, dvb_operations, dvb_admin, dbadmin, pgagent, dvb_user_readonly;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE DVB_CONFIG."_DVB_RUNTIME_DOCUMENTATION" TO dvb_user, dvb_operations, dvb_admin, dbadmin, pgagent, dvb_user_readonly;
 
GRANT SELECT ON TABLE DVB_CONFIG.AUTH_USERS TO dvb_user, dvb_operations, dvb_admin, dbadmin, pgagent, dvb_user_readonly;
GRANT SELECT ON TABLE DVB_CONFIG.CONFIG TO dvb_user, dvb_operations, dvb_admin, dbadmin, pgagent, dvb_user_readonly;
 
GRANT ALL PRIVILEGES ON SCHEMA DVB_LOG TO dvb_admin, dbadmin;
GRANT SELECT, UPDATE, INSERT ON SCHEMA DVB_LOG TO dvb_user, dvb_operations, dvb_admin, dbadmin, pgagent;
GRANT SELECT, INSERT ON SCHEMA DVB_LOG TO dvb_user_readonly;

GRANT ALL PRIVILEGES ON SCHEMA ACCESS_ERRORMART TO dvb_user, dvb_operations, dvb_admin, dbadmin, pgagent;
GRANT ALL PRIVILEGES ON SCHEMA ACCESSLAYER TO dvb_user, dvb_operations, dvb_admin, dbadmin, pgagent;
GRANT ALL PRIVILEGES ON SCHEMA BUSINESS_RULES TO dvb_user, dvb_operations, dvb_admin, dbadmin, pgagent;
GRANT ALL PRIVILEGES ON SCHEMA BUSINESSOBJECTS TO dvb_user, dvb_operations, dvb_admin, dbadmin, pgagent;
GRANT ALL PRIVILEGES ON SCHEMA DATAVAULT TO dvb_user, dvb_operations, dvb_admin, dbadmin, pgagent;
GRANT ALL PRIVILEGES ON SCHEMA DATAVAULT_STAGING TO dvb_user, dvb_operations, dvb_admin, dbadmin, pgagent;
GRANT ALL PRIVILEGES ON SCHEMA STAGING TO dvb_user, dvb_admin, dvb_operations, dbadmin, pgagent;

GRANT 
    CREATE ANY TABLE,
    ALTER ANY TABLE,
    DROP ANY TABLE,
    CREATE ANY VIEW,
    DROP ANY VIEW
  TO dvb_user, dvb_operations, dvb_admin, dbadmin, pgagent;
GRANT SELECT ANY DICTIONARY TO dvb_user, dvb_operations, dvb_admin, dbadmin, pgagent, dvb_user_readonly;
 
GRANT ALL PRIVILEGES ON SCHEMA ACCESSLAYER TO dvb_user, dvb_operations, dvb_admin, dbadmin, pgagent;
GRANT ALL PRIVILEGES ON SCHEMA BUSINESS_RULES TO dvb_user, dvb_operations, dvb_admin, dbadmin, pgagent;
GRANT ALL PRIVILEGES ON SCHEMA BUSINESSOBJECTS TO dvb_user, dvb_operations, dvb_admin, dbadmin, pgagent;
GRANT ALL PRIVILEGES ON SCHEMA DATAVAULT TO dvb_user, dvb_operations, dvb_admin, dbadmin, pgagent;
GRANT ALL PRIVILEGES ON SCHEMA DATAVAULT_STAGING TO dvb_user, dvb_operations, dvb_admin, dbadmin, pgagent;
GRANT ALL PRIVILEGES ON SCHEMA STAGING TO dvb_user, dvb_operations, dvb_admin, dbadmin, pgagent;

GRANT SELECT ON SCHEMA ACCESS_ERRORMART TO dvb_user_readonly;
GRANT SELECT ON SCHEMA ACCESSLAYER TO dvb_user_readonly;
GRANT SELECT ON SCHEMA BUSINESS_RULES TO dvb_user_readonly;
GRANT SELECT ON SCHEMA BUSINESSOBJECTS TO dvb_user_readonly;
GRANT SELECT ON SCHEMA DATAVAULT TO dvb_user_readonly;
GRANT SELECT ON SCHEMA DATAVAULT_STAGING TO dvb_user_readonly;
GRANT SELECT ON SCHEMA STAGING TO dvb_user_readonly;
 
GRANT EXECUTE ANY FUNCTION, EXECUTE ANY SCRIPT TO  dvb_user, dvb_operations, dvb_user_readonly, dvb_admin, dbadmin, pgagent;
GRANT ALL PRIVILEGES ON SCHEMA DVB_CORE TO dvb_admin, dbadmin;
GRANT SELECT, INSERT, UPDATE, DELETE ON SCHEMA DVB_CORE TO dvb_user, dvb_operations, pgagent;
GRANT SELECT ON SCHEMA DVB_CORE TO dvb_user_readonly;

GRANT IMPERSONATION ON 
    dvb_user,
    dvb_user_readonly,
    dvb_operations,
    dvb_admin,
    dbadmin,
    pgagent 
  TO authenticator;

MERGE INTO DVB_CONFIG.AUTH_USERS target USING (
  SELECT 'dvb' AS USERNAME,'dvb@example.com' AS EMAIL,'Development user' AS FULL_NAME,	'$2a$08$HI1QTUkC640DG1hcFyrgXu3nwRjgUXoFNMi.iVFjUNbKtmu7.gMKy' AS PASSWORD_HASH, NULL AS PASSWORD_EXPIRATION, 'dvb_admin' AS PG_USER, TRUE AS NON_EXPIRING_TOKEN FROM DUAL
) sourcedata	
ON (sourcedata.USERNAME = target.USERNAME)
WHEN NOT MATCHED THEN
    INSERT (USERNAME, EMAIL, FULL_NAME, PASSWORD_HASH, PASSWORD_EXPIRATION, PG_USER, NON_EXPIRING_TOKEN)
    VALUES (sourcedata.USERNAME, sourcedata.EMAIL, sourcedata.FULL_NAME, sourcedata.PASSWORD_HASH, sourcedata.PASSWORD_EXPIRATION, sourcedata.PG_USER, sourcedata.NON_EXPIRING_TOKEN);

COMMIT;