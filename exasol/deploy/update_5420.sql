-- Deploy dvb:update_5420 to exasol


CREATE OR REPLACE VIEW "DVB_CORE"."VIEWS" AS WITH view_sub_sub_query AS (
SELECT
    VIEW_SCHEMA,
    VIEW_NAME,
    VIEW_COMMENT,
    VIEW_TEXT,
    REGEXP_REPLACE(VIEW_TEXT, '(*CRLF)COMMENT IS.*(?![^\(]*\))|;') AS VIEW_CODE_WO_COMMENT_IS
    -- remove view comment part

    FROM SYS.EXA_ALL_VIEWS av
WHERE
    av.VIEW_SCHEMA IN ('STAGING',
    'DATAVAULT_STAGING',
    'DATAVAULT',
    'BUSINESSOBJECTS',
    'BUSINESS_RULES',
    'ACCESSLAYER',
    'ACCESS_ERRORMART')),
view_sub_query AS (
SELECT
    VIEW_SCHEMA,
    VIEW_NAME,
    VIEW_COMMENT,
    VIEW_TEXT,
    RTRIM( REGEXP_REPLACE(VIEW_CODE_WO_COMMENT_IS, '(?x)(?s)(?U)\R/\*[\{\[].*[\}\]]\*/\s*$', ''), '; ' || CHR(9) || CHR(10) || CHR(13)) AS VIEW_CODE_PART,
    NULLIF( REGEXP_REPLACE(VIEW_CODE_WO_COMMENT_IS, '(?x)(?s)(?U)^.*\R/\*\s*([\{\[].*[\}\]])\s*\*/\s*$', '\1'),
    VIEW_CODE_WO_COMMENT_IS ) AS VIEW_INLINE_JSON
FROM
    view_sub_sub_query )
SELECT
    CONCAT(v.VIEW_SCHEMA, '.', v.VIEW_NAME) AS view_id,
    v.VIEW_NAME AS view_nq_id,
    v.VIEW_SCHEMA AS schema_id,
    dvb_core.f_get_schema_name(v.VIEW_SCHEMA) AS schema_name,
    FALSE AS view_is_materialized,
    --no materizlized views in exasol
 LTRIM(REGEXP_REPLACE(v.VIEW_CODE_PART, '(*CRLF)(?s)(?i).*?' || v.VIEW_NAME || '.*?AS(?![^\n]*'')', '', 1, 1), ' ' || CHR(9) || CHR(10) || CHR(13)) AS view_code,
    -- takes out only the select code of the view definition by removing the create statement
 JSON_VALUE(v.VIEW_COMMENT,
    '$.name') AS metadata_name,
    JSON_VALUE(v.VIEW_COMMENT,
    '$.comment') AS metadata_comment,
    CASE WHEN (v.VIEW_SCHEMA != 'DVB_CORE') THEN VIEW_INLINE_JSON
    ELSE NULL
END AS metadata_businessobject_structure,
JSON_VALUE(v.VIEW_COMMENT,
'$.quick_inserts.json()') AS metadata_quick_inserts,
LTRIM(v.view_code_part, ' ' || CHR(9) || CHR(10) || CHR(13)) AS metadata_code,
CAST(JSON_VALUE(v.VIEW_COMMENT,
'$.is_error_ruleset') AS BOOLEAN) AS metadata_is_error_ruleset,
CAST(JSON_VALUE(v.VIEW_COMMENT,
'$.include_in_accesslayer') AS BOOLEAN) AS metadata_include_in_accesslayer,
CAST(JSON_VALUE(v.VIEW_COMMENT,
'$.accesslayer_priorization') AS NUMBER) AS metadata_accesslayer_priorization
FROM
view_sub_query AS v;

COMMIT;
