set define off;
set escape off;

CREATE SCHEMA IF NOT EXISTS BUSINESSOBJECTS_PITS;
GRANT ALL PRIVILEGES ON SCHEMA BUSINESSOBJECTS_PITS TO dvb_user, dvb_operations, dvb_admin, dbadmin, pgagent;
GRANT ALL PRIVILEGES ON SCHEMA BUSINESSOBJECTS_PITS TO dvb_user, dvb_operations, dvb_admin, dbadmin, pgagent;
GRANT SELECT ON SCHEMA BUSINESSOBJECTS_PITS TO dvb_user_readonly;

CREATE OR REPLACE VIEW "DVB_CORE"."STAGING_TABLES" AS 
SELECT
	st.staging_table_id,
  st.staging_table_name,
  st.staging_table_display_string,
  st.staging_table_comment,
  st.staging_table_type_name,
  st.staging_table_type_id,
  st.schema_id,
  st.schema_name,
  st.system_id,
  s.system_name,
  s.system_color,
  s.system_comment,
  st.source_table_id,
  st.source_name,
  st.source_table_type_id,
  st.source_object_id,
  st.source_schema_id,
  st.source_query_customized,
  st.batch_size,
  st.where_clause_general_part,
  st.where_clause_delta_part_template,
  st.is_delta_load,
  st.is_up_to_date,
  st.applied_where_clause_general_part,
  st.applied_where_clause_delta_part,
  st.applied_where_clause_delta_part_template,
  st.data_extract_start_time
FROM (
	SELECT
	    concat(ao.ROOT_NAME ,'.' , ao.OBJECT_NAME) AS staging_table_id,
	    JSON_VALUE(ao.OBJECT_COMMENT,'$.name') AS staging_table_name,
	    CASE
	      WHEN JSON_VALUE(ao.OBJECT_COMMENT,'$.name') IS NOT NULL THEN
	        JSON_VALUE(ao.OBJECT_COMMENT,'$.name')
	      ELSE
	        upper(replace (dvb_core.f_string_between(ao.OBJECT_NAME, '_R_', ''), '_', ' '))
	    END || COALESCE(NULLIF(' (' || dvb_core.f_string_between(ao.OBJECT_NAME, '_U_', '') || ')', ' ()'), '') 
	    	|| ' [' || ao.OBJECT_NAME || ']' AS staging_table_display_string,
	    JSON_VALUE(ao.OBJECT_COMMENT,'$.comment') AS staging_table_comment,
	    CASE ao.OBJECT_TYPE
	      WHEN 'TABLE' THEN 'Table'
	      WHEN 'VIEW' THEN 'View'
	      ELSE NULL
	    END AS staging_table_type_name,
	    CASE ao.OBJECT_TYPE
	      WHEN 'TABLE' THEN 'r'
	      WHEN 'VIEW' THEN 'v'
	      ELSE NULL
	    END AS staging_table_type_id,
	    ao.ROOT_NAME AS schema_id,
	    dvb_core.f_get_schema_name(ao.ROOT_NAME) AS schema_name,
	    dvb_core.f_string_between(ao.OBJECT_NAME, '', '_R_') AS system_id,
	    JSON_VALUE(ao.OBJECT_COMMENT,'$.source_table_id') AS source_table_id,
	    JSON_VALUE(ao.OBJECT_COMMENT,'$.name') AS source_name,
	    JSON_VALUE(ao.OBJECT_COMMENT,'$.source_table_type') AS source_table_type_id,
	    COALESCE(dvb_core.f_string_between(ao.OBJECT_NAME, '_R_', '_U_'), dvb_core.f_string_between(ao.OBJECT_NAME, '_R_', ''), '') AS source_object_id,
	    COALESCE(dvb_core.f_string_between(ao.OBJECT_NAME, '_U_', ''), '') AS source_schema_id,
	    JSON_VALUE(ao.OBJECT_COMMENT,'$.source_query_customized') AS source_query_customized,
	    COALESCE(TO_NUMBER(JSON_VALUE(ao.OBJECT_COMMENT,'$.batch_size')),-1) AS batch_size,
	    JSON_VALUE(ao.OBJECT_COMMENT,'$.where_clause_general_part') AS where_clause_general_part,
      JSON_VALUE(ao.OBJECT_COMMENT,'$.where_clause_delta_part_template') AS where_clause_delta_part_template,
	    COALESCE(rts.is_delta_load, FALSE) AS is_delta_load,
	    COALESCE(rts.is_up_to_date, FALSE) AS is_up_to_date,
		rts.applied_where_clause_general_part,
		rts.applied_where_clause_delta_part,
		rts.applied_where_clause_delta_part_template,
		rts.data_extract_start_time
	  FROM SYS.EXA_ALL_OBJECTS ao
	  LEFT JOIN staging."_DVB_RUNTIME_TABLE_STATUS" rts ON rts.STAGING_TABLE_ID = (CONCAT( ao.ROOT_NAME , '.' ,ao.OBJECT_NAME))
	  WHERE ao.ROOT_NAME = 'STAGING' 
	    AND (ao.OBJECT_TYPE IN ('TABLE', 'VIEW' /*,'MATERIALIZED VIEW' --no such thing in exasol*/)) 
	    AND  ao.OBJECT_NAME NOT LIKE '\_DVB\_%' ESCAPE '\' 
	    AND  ao.OBJECT_NAME NOT LIKE 'T\_%' ESCAPE '\'
  ) st
  LEFT JOIN dvb_core.systems s ON s.SYSTEM_ID = st.system_id
	WHERE st.system_id IS NOT NULL;


CREATE OR REPLACE VIEW "DVB_CORE"."VIEWS" AS WITH view_sub_sub_query AS (
SELECT
    VIEW_SCHEMA,
    VIEW_NAME,
    VIEW_COMMENT,
    VIEW_TEXT,
    REGEXP_REPLACE(VIEW_TEXT, '(*CRLF)COMMENT IS.*(?![^\(]*\))|;') AS VIEW_CODE_WO_COMMENT_IS
    -- remove view comment part

    FROM SYS.EXA_ALL_VIEWS av
WHERE
    av.VIEW_SCHEMA IN ('STAGING',
    'DATAVAULT_STAGING',
    'DATAVAULT',
    'BUSINESSOBJECTS',
    'BUSINESSOBJECTS_PITS',
    'BUSINESS_RULES',
    'ACCESSLAYER',
    'ACCESS_ERRORMART')),
view_sub_query AS (
SELECT
    VIEW_SCHEMA,
    VIEW_NAME,
    VIEW_COMMENT,
    VIEW_TEXT,
    RTRIM( REGEXP_REPLACE(VIEW_CODE_WO_COMMENT_IS, '(?x)(?s)(?U)\R/\*[\{\[].*[\}\]]\*/\s*$', ''), '; ' || CHR(9) || CHR(10) || CHR(13)) AS VIEW_CODE_PART,
    NULLIF( REGEXP_REPLACE(VIEW_CODE_WO_COMMENT_IS, '(?x)(?s)(?U)^.*\R/\*\s*([\{\[].*[\}\]])\s*\*/\s*$', '\1'),
    VIEW_CODE_WO_COMMENT_IS ) AS VIEW_INLINE_JSON
FROM
    view_sub_sub_query )
SELECT
    CONCAT(v.VIEW_SCHEMA, '.', v.VIEW_NAME) AS view_id,
    v.VIEW_NAME AS view_nq_id,
    v.VIEW_SCHEMA AS schema_id,
    dvb_core.f_get_schema_name(v.VIEW_SCHEMA) AS schema_name,
    FALSE AS view_is_materialized,
    --no materizlized views in exasol
 LTRIM(REGEXP_REPLACE(v.VIEW_CODE_PART, '(*CRLF)(?s)(?i).*?' || v.VIEW_NAME || '.*?AS(?![^\n]*'')', '', 1, 1), ' ' || CHR(9) || CHR(10) || CHR(13)) AS view_code,
    -- takes out only the select code of the view definition by removing the create statement
 JSON_VALUE(v.VIEW_COMMENT,
    '$.name') AS metadata_name,
    JSON_VALUE(v.VIEW_COMMENT,
    '$.comment') AS metadata_comment,
    CASE WHEN (v.VIEW_SCHEMA != 'DVB_CORE') THEN VIEW_INLINE_JSON
    ELSE NULL
END AS metadata_businessobject_structure,
JSON_VALUE(v.VIEW_COMMENT,
'$.quick_inserts.json()') AS metadata_quick_inserts,
LTRIM(v.view_code_part, ' ' || CHR(9) || CHR(10) || CHR(13)) AS metadata_code,
CAST(JSON_VALUE(v.VIEW_COMMENT,
'$.is_error_ruleset') AS BOOLEAN) AS metadata_is_error_ruleset,
CAST(JSON_VALUE(v.VIEW_COMMENT,
'$.include_in_accesslayer') AS BOOLEAN) AS metadata_include_in_accesslayer,
CAST(JSON_VALUE(v.VIEW_COMMENT,
'$.accesslayer_priorization') AS NUMBER) AS metadata_accesslayer_priorization,
	v.VIEW_COMMENT AS description
FROM
view_sub_query AS v;

CREATE OR REPLACE VIEW "DVB_CORE"."BUSINESSOBJECTS" AS WITH view_sub_sub_query AS (
SELECT
    VIEW_SCHEMA,
    VIEW_NAME,
    VIEW_COMMENT,
    VIEW_TEXT,
    COALESCE(dvb_core.f_string_between_ci(VIEW_TEXT,
    '',
    'COMMENT IS '''),
    VIEW_TEXT) AS VIEW_CODE_WO_COMMENT_IS
FROM
    SYS.EXA_ALL_VIEWS
WHERE
    VIEW_SCHEMA = 'BUSINESSOBJECTS' ),
view_sub_query AS (
SELECT
    VIEW_SCHEMA,
    VIEW_NAME,
    VIEW_COMMENT,
    VIEW_TEXT,
    RTRIM( REGEXP_REPLACE(VIEW_CODE_WO_COMMENT_IS, '(?x)(?s)(?U)\R/\*[\{\[].*[\}\]]\*/\s*$', ''), '; ' || CHR(9) || CHR(10) || CHR(13)) || '
;' AS VIEW_CODE_PART,
    NULLIF( REGEXP_REPLACE(VIEW_CODE_WO_COMMENT_IS, '(?x)(?s)(?U)^.*\R\/\*\s*([\{\[].*[\}\]])\s*\*\/\s*;*\s*$', '\1'),
    VIEW_CODE_WO_COMMENT_IS ) AS VIEW_INLINE_JSON
FROM
    view_sub_sub_query )
SELECT
    b.businessobject_view_id,
    b.functional_suffix_id,
    b.functional_suffix_name,
    b.system_id,
    b.system_name,
    b.system_color,
    b.system_comment,
    b.businessobject_comment,
    b.start_hub_id,
    b.start_hub_name,
    b.start_hub_name || ' > ' || b.system_name || ' > ' || COALESCE( NULLIF(b.functional_suffix_name,
    ''),
    'Default' ) AS businessobject_name,
    b.businessobject_structure,
    b.scd_type
FROM
    (
    SELECT
        bo.businessobject_view_id,
        bo.functional_suffix_id,
        bo.functional_suffix_name,
        COALESCE(s.system_id,
        bo.system_id) AS system_id,
        s.system_name,
        s.system_color,
        s.system_comment,
        bo.businessobject_comment,
        bo.start_hub_id,
        JSON_VALUE(ext.OBJECT_COMMENT,
        '$.name') AS start_hub_name,
        bo.businessobject_structure,
        CASE RIGHT(bo.businessobject_view_id, 2) 
          WHEN '_H' THEN 2 
          WHEN '_C' THEN 1
          WHEN '_O' THEN 0
          WHEN '_A' THEN 4
          ELSE  1
        END AS scd_type
    FROM
        (
        SELECT
            v.view_name,
            v.view_schema || '.' || v.view_name AS businessobject_view_id,
            COALESCE(dvb_core.f_string_between(v.view_name,
            '_C_',
            ''),
            '') AS functional_suffix_id,
            JSON_VALUE(v.VIEW_COMMENT,
            '$.name') AS functional_suffix_name,
            COALESCE(dvb_core.f_string_between(v.view_name,
            '_S_',
            '_C_'),
            dvb_core.f_string_between(v.view_name,
            '_S_',
            '')) AS system_id,
            JSON_VALUE(v.VIEW_COMMENT,
            '$.comment') AS businessobject_comment,
            'H_' || dvb_core.f_string_between(v.view_name,
            '',
            '_S_') AS start_hub_id,
            VIEW_INLINE_JSON AS businessobject_structure
        FROM
            view_sub_query v) bo
    LEFT JOIN dvb_core.systems s ON
        (s.system_id = bo.system_id)
    LEFT JOIN SYS.EXA_ALL_OBJECTS ext ON
        (ext.ROOT_NAME || '.' || ext.OBJECT_NAME) = ('DATAVAULT.H_' || dvb_core.f_string_between(bo.view_name,
        '',
        '_S_')) ) b;

		
CREATE OR REPLACE VIEW "DVB_CORE"."BUSINESS_RULES" AS SELECT
        a.business_ruleset_view_id,
        a.functional_suffix_id,
        a.functional_suffix_name,
        a.business_ruleset_suffix_id,
        a.business_ruleset_suffix_name,
        a.system_id,
        a.system_name,
        a.system_color,
        a.system_comment,
        a.start_hub_id,
        a.start_hub_name,
        a.related_businessobject_view_id,
        a.business_rules_comment,
        a.start_hub_name || ' > ' || a.system_name || ' > ' || COALESCE(NULLIF(a.functional_suffix_name, ''), 'Default') || ' > ' || COALESCE(NULLIF(a.business_ruleset_suffix_name, ''), 'Default') AS business_ruleset_name,
        a.business_rules_view_code,
        a.is_error_ruleset,
        a.include_in_accesslayer,
        a.accesslayer_priorization,
        a.quick_inserts,
		a.alias_schema_id,
		a.alias_view_nq_id
    FROM
        (
            SELECT
                iv.view_id AS business_ruleset_view_id,
                COALESCE(dvb_core.f_string_between(iv.view_nq_id, '_C_', '_B_'), dvb_core.f_string_between(iv.view_nq_id, '_C_', ''), '') AS functional_suffix_id,
                JSON_VALUE(ext2.OBJECT_COMMENT,'$.name') AS functional_suffix_name,
                COALESCE(
                    dvb_core.f_string_between(iv.view_nq_id, '_B_', ''),
                    ''
                ) AS business_ruleset_suffix_id,
                iv.metadata_name AS business_ruleset_suffix_name,
                s.system_id,
                s.system_name,
                s.system_color,
                s.system_comment,
                'H_' || dvb_core.f_string_between(iv.view_nq_id, '', '_S_') AS start_hub_id,
                 JSON_VALUE(ext.OBJECT_COMMENT,'$.name') AS start_hub_name,
                'BUSINESSOBJECTS.' || COALESCE(
                    dvb_core.f_string_between(iv.view_nq_id, '', '_B_'),
                    iv.view_nq_id
                ) AS related_businessobject_view_id,
                iv.metadata_comment AS business_rules_comment,
                iv.metadata_code AS business_rules_view_code,
                iv.metadata_is_error_ruleset AS is_error_ruleset,
                iv.metadata_include_in_accesslayer AS include_in_accesslayer,
                iv.metadata_accesslayer_priorization AS accesslayer_priorization,
                iv.metadata_quick_inserts AS quick_inserts,
				JSON_VALUE(iv.description, '$.alias_schema_id') AS alias_schema_id,
				JSON_VALUE(iv.description, '$.alias_view_nq_id') AS alias_view_nq_id
            FROM dvb_core.views iv
              LEFT JOIN SYS.EXA_ALL_OBJECTS ext ON (ext.ROOT_NAME || '.'  || ext.OBJECT_NAME) = ('DATAVAULT.H_' || dvb_core.f_string_between(iv.view_nq_id, '', '_S_'))
              LEFT JOIN SYS.EXA_ALL_OBJECTS ext2 ON (ext2.ROOT_NAME || '.'  || ext2.OBJECT_NAME) = ('BUSINESSOBJECTS.' || COALESCE(dvb_core.f_string_between(iv.view_nq_id, '', '_B_'), iv.view_nq_id))
              LEFT JOIN dvb_core.systems s ON
                s.system_id = COALESCE(
                    dvb_core.f_string_between(iv.view_nq_id, '_S_', '_C_'),
                    dvb_core.f_string_between(iv.view_nq_id, '_S_', '_B_'),
                    dvb_core.f_string_between(iv.view_nq_id, '_S_', '')
                )
            WHERE
                iv.schema_id = 'BUSINESS_RULES'
        ) a
    WHERE
        a.system_id IS NOT NULL;
		
		

CREATE OR REPLACE VIEW "DVB_CORE"."COLUMNS" AS SELECT
    col.schema_id,
    col.table_nq_id,
    col.column_nq_id,
    col.column_id,
    col.column_name,
    col.column_comment,
    col.column_conversion_type,
    col.data_type,
    col.data_type_id,
    col.character_maximum_length,
    col.numeric_precision,
    col.numeric_scale,
    col.datetime_precision,
    col.ordinal_position,
    col.complete_data_type,
    col.description
FROM
    (
        SELECT DISTINCT
        --filter duplicates from materialized views and tables
 atc.COLUMN_SCHEMA AS schema_id,
        atc.COLUMN_TABLE AS table_nq_id,
        atc.COLUMN_NAME AS column_nq_id,
        atc.COLUMN_SCHEMA || '.' || atc.COLUMN_TABLE || '.' || atc.COLUMN_NAME AS column_id,
        JSON_VALUE(atc.COLUMN_COMMENT,
        '$.name') AS column_name,
        JSON_VALUE(atc.COLUMN_COMMENT,
        '$.comment') AS column_comment,
        JSON_VALUE(atc.COLUMN_COMMENT,
        '$.conversion_type.json()') AS column_conversion_type,
        CASE
            WHEN atc.COLUMN_TYPE = 'DOUBLE' THEN 'DOUBLE PRECISION'
            ELSE REGEXP_REPLACE(atc.COLUMN_TYPE, '\(\d+\)|\(\d+,\d+\)', '')
        END AS data_type,
        COLUMN_TYPE_ID AS data_type_id,
        atc.COLUMN_MAXSIZE AS character_maximum_length,
        atc.COLUMN_NUM_PREC AS numeric_precision,
        atc.COLUMN_NUM_SCALE AS numeric_scale,
        CASE
            WHEN atc.COLUMN_TYPE LIKE '%TIMESTAMP%' THEN atc.COLUMN_NUM_SCALE
            ELSE NULL
        END AS datetime_precision,
        atc.COLUMN_ORDINAL_POSITION AS ordinal_position,
        CASE
            WHEN atc.COLUMN_TYPE = 'DOUBLE' THEN 'DOUBLE PRECISION'
            ELSE atc.COLUMN_TYPE
        END ||
        CASE
            WHEN atc.COLUMN_TYPE = 'NUMBER'
            AND atc.COLUMN_NUM_PREC IS NOT NULL THEN '(' || atc.COLUMN_NUM_PREC || ',' || atc.COLUMN_NUM_SCALE || ')'
            ELSE NULL
        END AS complete_data_type,
        atc.COLUMN_COMMENT AS DESCRIPTION
    FROM
        SYS.EXA_ALL_COLUMNS atc
    JOIN SYS.EXA_ALL_OBJECTS ao ON
        (ao.ROOT_NAME = atc.COLUMN_SCHEMA
        AND ao.OBJECT_NAME = atc.COLUMN_TABLE)
    WHERE
        atc.COLUMN_SCHEMA IN ('STAGING',
        'DATAVAULT_STAGING',
        'DATAVAULT',
		'BUSINESSOBJECTS_PITS',
        'BUSINESSOBJECTS',
        'BUSINESS_RULES',
        'ACCESSLAYER',
        'ACCESS_ERRORMART')
        AND ao.OBJECT_TYPE IN ('TABLE',
        'VIEW' /*,'MATERIALIZED VIEW' --no such thing in exasol*/
        ) ) col;
		
		
CREATE OR REPLACE VIEW "DVB_CORE"."TABLES" AS SELECT
    sub.table_id,
    sub.table_nq_id,
    sub.schema_id,
    sub.schema_name,
    sub.table_name,
    sub.table_comment,
    sub.type_id,
    sub.type_name,
    sub.system_id,
    s.system_name,
    s.system_color,
    s.system_comment
FROM
    (
    SELECT
        DISTINCT c.ROOT_NAME || '.' || c.OBJECT_NAME AS table_id,
        c.OBJECT_NAME AS table_nq_id,
        c.ROOT_NAME AS schema_id,
        dvb_core.f_get_schema_name(c.ROOT_NAME) AS schema_name,
        COALESCE( CAST(a.accesslayer_name AS VARCHAR(1000)),
        CAST(ae.access_errormart_name AS VARCHAR(1000)),
        CAST(br.business_ruleset_name AS VARCHAR(1000)),
        CAST(bl.businessobject_name AS VARCHAR(1000)),
        CAST(h.hub_name AS VARCHAR(1000)),
        CAST(s_1.satellite_name AS VARCHAR(1000)),
        CAST(ls.linksatellite_name AS VARCHAR(1000)),
        CAST(l.link_name AS VARCHAR(1000)),
        CAST(ts.tracking_satellite_name AS VARCHAR(1000)),
        CAST(tl.transaction_link_name AS VARCHAR(1000)),
        CAST(st.staging_table_name AS VARCHAR(1000)),
        'N/A' ) AS table_name,
        JSON_VALUE(c.OBJECT_COMMENT,
        '$.comment') AS table_comment,
        CASE
            WHEN c.OBJECT_TYPE = 'TABLE' THEN CAST('r' AS CHAR(1))
            WHEN c.OBJECT_TYPE = 'VIEW' THEN CAST('v' AS CHAR(1))
            ELSE CAST(NULL AS CHAR(1))
        END AS type_id,
        CASE
            WHEN c.OBJECT_TYPE = 'TABLE' THEN 'Base Table'
            WHEN c.OBJECT_TYPE = 'VIEW' THEN 'View'
            --WHEN 'm' THEN 'Materialized View'
            ELSE CAST(NULL AS VARCHAR(1))
        END AS type_name,
        CASE WHEN S_1.SATELLITE_IS_PROTOTYPE THEN 'DVB_PROTOTYPE' ELSE
        COALESCE( dvb_core.f_string_between(c.OBJECT_NAME,
        '_S_',
        '_R_'),
        dvb_core.f_string_between(c.OBJECT_NAME,
        '_S_',
        '_C_'),
        dvb_core.f_string_between(c.OBJECT_NAME,
        '_S_',
        ''),
        dvb_core.f_string_between('^' || c.OBJECT_NAME,
        '^T_',
        '_R_'),
        dvb_core.f_string_between(c.OBJECT_NAME,
        '',
        '_R_')
        -- in case of a staging table
) END AS system_id
    FROM
        SYS.EXA_ALL_OBJECTS c
        --select * from SYS.EXA_ALL_OBJECTS
    LEFT JOIN dvb_core.accesslayers a ON
        a.accesslayer_id = c.ROOT_NAME || '.' || c.OBJECT_NAME
    LEFT JOIN dvb_core.access_errormart ae ON
        ae.access_errormart_id = c.ROOT_NAME || '.' || c.OBJECT_NAME
    LEFT JOIN dvb_core.business_rules br ON
        br.business_ruleset_view_id = c.ROOT_NAME || '.' || c.OBJECT_NAME
    LEFT JOIN dvb_core.businessobjects bl ON
        bl.businessobject_view_id = c.ROOT_NAME || '.' || c.OBJECT_NAME
    LEFT JOIN dvb_core.hubs h ON
        c.ROOT_NAME = 'DATAVAULT'
        AND h.hub_id = c.OBJECT_NAME
    LEFT JOIN dvb_core.links l ON
        c.ROOT_NAME = 'DATAVAULT'
        AND l.link_id = c.OBJECT_NAME
    LEFT JOIN dvb_core.satellites s_1 ON
        c.ROOT_NAME = 'DATAVAULT'
        AND s_1.satellite_id = LEFT(c.OBJECT_NAME,
        LEN(c.OBJECT_NAME)- 2)
    LEFT JOIN dvb_core.linksatellites ls ON
        c.ROOT_NAME = 'DATAVAULT'
        AND ls.linksatellite_id = LEFT(c.OBJECT_NAME,
        LEN(c.OBJECT_NAME)- 2)
    LEFT JOIN dvb_core.tracking_satellites ts ON
        c.ROOT_NAME = 'DATAVAULT'
        AND ts.tracking_satellite_id = LEFT(c.OBJECT_NAME,
        LEN(c.OBJECT_NAME)- 2)
    LEFT JOIN dvb_core.transaction_links tl ON
        c.ROOT_NAME = 'DATAVAULT'
        AND tl.transaction_link_id = LEFT(c.OBJECT_NAME,
        LEN(c.OBJECT_NAME)- 2)
    LEFT JOIN dvb_core.staging_tables st ON
        st.staging_table_id = c.ROOT_NAME || '.' || c.OBJECT_NAME
    WHERE
        c.ROOT_NAME IN( 'STAGING',
        'DATAVAULT_STAGING',
        'DATAVAULT',
		'BUSINESSOBJECTS_PITS',
        'BUSINESSOBJECTS',
        'BUSINESS_RULES',
        'ACCESSLAYER',
        'ACCESS_ERRORMART' )
        --AND c.OBJECT_TYPE NOT IN ('U', 'V')
        AND c.OBJECT_NAME NOT LIKE '\_DVB\_%' ESCAPE '\' ) sub
LEFT JOIN dvb_core.systems s ON
    s.system_id = sub.system_id;
	

CREATE OR REPLACE VIEW "DVB_CORE"."TABLES_SIMPLE" AS SELECT
    ao.ROOT_NAME || '.' || ao.OBJECT_NAME AS TABLE_ID,
    ao.ROOT_NAME AS SCHEMA_ID,
    ao.OBJECT_NAME AS TABLE_NQ_ID,
    CASE
        ao.OBJECT_TYPE
        WHEN 'TABLE' THEN 'r'
        WHEN 'VIEW' THEN 'v'
        ELSE NULL
    END AS TYPE_ID,
    CASE
        ao.OBJECT_TYPE
        WHEN 'TABLE' THEN 'Base Table'
        WHEN 'VIEW' THEN 'View'
        ELSE NULL
    END AS TYPE_NAME,
    CASE 
    	WHEN ao.OBJECT_NAME LIKE '%\_P' THEN 'DVB_PROTOTYPE'
    	WHEN ao.OBJECT_NAME LIKE 'T\_%' THEN NULL
    	ELSE
    COALESCE(dvb_core.f_string_between(ao.OBJECT_NAME,
    '_S_',
    '_R_'),
    dvb_core.f_string_between(ao.OBJECT_NAME,
    '_S_',
    '_C_'),
    dvb_core.f_string_between(ao.OBJECT_NAME,
    '_S_',
    ''),
    dvb_core.f_string_between(ao.OBJECT_NAME,
    '',
    '_R_')) END AS SYSTEM_ID,
    OBJECT_COMMENT AS TABLE_COMMENT
FROM
    SYS.EXA_ALL_OBJECTS ao
WHERE
    ao.ROOT_NAME IN ('STAGING',
    'DATAVAULT_STAGING',
    'DATAVAULT',
	'BUSINESSOBJECTS_PITS',
    'BUSINESSOBJECTS',
    'BUSINESS_RULES',
    'ACCESSLAYER',
    'ACCESS_ERRORMART')
    AND ao.OBJECT_TYPE IN ('TABLE',
    'VIEW',
    'MATERIALIZED VIEW')
    AND ao.OBJECT_NAME NOT LIKE '\_DVB\_%' ESCAPE '\';


CREATE OR REPLACE VIEW "DVB_CORE"."VIEW_RELATIONS" AS SELECT
    CONCAT(REFERENCED_OBJECT_SCHEMA, '.', dep.REFERENCED_OBJECT_NAME) AS table_id,
    dep.REFERENCED_OBJECT_SCHEMA AS table_schema_id,
    dep.REFERENCED_OBJECT_NAME AS table_nq_id,
    CASE
        WHEN dep.REFERENCED_OBJECT_TYPE = 'TABLE' THEN CAST('r' AS CHAR(1))
        WHEN dep.REFERENCED_OBJECT_TYPE = 'VIEW' THEN CAST('v' AS CHAR(1))
    END AS table_type_id,
    CONCAT(dep.OBJECT_SCHEMA, '.', dep.OBJECT_NAME) AS dependent_view_id,
    dep.OBJECT_SCHEMA AS dependent_view_schema_id,
    dep.OBJECT_NAME AS dependent_view_nq_id,
    CAST(LEFT(LOWER(dep.OBJECT_TYPE),
    1) AS CHAR(1)) AS dependent_view_type_id
FROM
    SYS.EXA_DBA_DEPENDENCIES_RECURSIVE dep
WHERE
    dep.REFERENCED_OBJECT_TYPE IN ('TABLE',
    'VIEW')
    AND dep.OBJECT_SCHEMA IN ('STAGING',
    'DATAVAULT_STAGING',
    'DATAVAULT',
    'BUSINESSOBJECTS_PITS',
    'BUSINESSOBJECTS',
    'BUSINESS_RULES',
    'ACCESSLAYER',
    'ACCESS_ERRORMART')
    AND dep.OBJECT_TYPE = 'VIEW'
    AND dep.DEPENDENCY_LEVEL = 1;

		

COMMIT;