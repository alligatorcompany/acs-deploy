set define off;
set escape off;
ALTER TABLE DVB_CONFIG.AUTH_USERS ADD COLUMN IF NOT EXISTS MIXED_API_AUTH BOOLEAN DEFAULT FALSE NOT NULL;

CREATE OR REPLACE VIEW "DVB_CORE"."COLUMNS" AS SELECT
	DISTINCT
    col.schema_id,
    col.table_nq_id,
    col.column_nq_id,
    col.column_id,
    col.column_name,
    col.column_comment,
    col.column_conversion_type,
    col.data_type,
    col.data_type_id,
    col.character_maximum_length,
    col.numeric_precision,
    col.numeric_scale,
    col.datetime_precision,
    col.ordinal_position,
    col.complete_data_type,
    col.description
FROM
    (
        SELECT atc.COLUMN_SCHEMA AS schema_id,
        atc.COLUMN_TABLE AS table_nq_id,
        atc.COLUMN_NAME AS column_nq_id,
        atc.COLUMN_SCHEMA || '.' || atc.COLUMN_TABLE || '.' || atc.COLUMN_NAME AS column_id,
        JSON_VALUE(atc.COLUMN_COMMENT,
        '$.name') AS column_name,
        JSON_VALUE(atc.COLUMN_COMMENT,
        '$.comment') AS column_comment,
        JSON_VALUE(atc.COLUMN_COMMENT,
        '$.conversion_type.json()') AS column_conversion_type,
        CASE
            WHEN atc.COLUMN_TYPE = 'DOUBLE' THEN 'DOUBLE PRECISION'
            ELSE REGEXP_REPLACE(atc.COLUMN_TYPE, '\(\d+\)|\(\d+,\d+\)', '')
        END AS data_type,
        COLUMN_TYPE_ID AS data_type_id,
        atc.COLUMN_MAXSIZE AS character_maximum_length,
        atc.COLUMN_NUM_PREC AS numeric_precision,
        atc.COLUMN_NUM_SCALE AS numeric_scale,
        CASE
            WHEN atc.COLUMN_TYPE LIKE '%TIMESTAMP%' THEN atc.COLUMN_NUM_SCALE
            ELSE NULL
        END AS datetime_precision,
        atc.COLUMN_ORDINAL_POSITION AS ordinal_position,
        CASE
            WHEN atc.COLUMN_TYPE = 'DOUBLE' THEN 'DOUBLE PRECISION'
            ELSE atc.COLUMN_TYPE
        END ||
        CASE
            WHEN atc.COLUMN_TYPE = 'NUMBER'
            AND atc.COLUMN_NUM_PREC IS NOT NULL THEN '(' || atc.COLUMN_NUM_PREC || ',' || atc.COLUMN_NUM_SCALE || ')'
            ELSE NULL
        END AS complete_data_type,
        atc.COLUMN_COMMENT AS DESCRIPTION
    FROM
        SYS.EXA_ALL_COLUMNS atc
    WHERE
        atc.COLUMN_SCHEMA IN ('STAGING',
        'DATAVAULT_STAGING',
        'DATAVAULT',
		'BUSINESSOBJECTS_PITS',
        'BUSINESSOBJECTS',
        'BUSINESS_RULES',
        'ACCESSLAYER',
        'ACCESS_ERRORMART')
        AND atc.COLUMN_OBJECT_TYPE IN ('TABLE',
        'VIEW' /*,'MATERIALIZED VIEW' --no such thing in exasol*/
        ) ) col;
		
CREATE OR REPLACE VIEW "DVB_CORE"."VIEW_RELATIONS" AS SELECT
    CONCAT(REFERENCED_OBJECT_SCHEMA, '.', dep.REFERENCED_OBJECT_NAME) AS table_id,
    dep.REFERENCED_OBJECT_SCHEMA AS table_schema_id,
    dep.REFERENCED_OBJECT_NAME AS table_nq_id,
    CASE
        WHEN dep.REFERENCED_OBJECT_TYPE = 'TABLE' THEN CAST('r' AS CHAR(1))
        WHEN dep.REFERENCED_OBJECT_TYPE = 'VIEW' THEN CAST('v' AS CHAR(1))
    END AS table_type_id,
    CONCAT(dep.OBJECT_SCHEMA, '.', dep.OBJECT_NAME) AS dependent_view_id,
    dep.OBJECT_SCHEMA AS dependent_view_schema_id,
    dep.OBJECT_NAME AS dependent_view_nq_id,
    CAST(LEFT(LOWER(dep.OBJECT_TYPE),
    1) AS CHAR(1)) AS dependent_view_type_id
FROM
    SYS.EXA_DBA_DEPENDENCIES dep
WHERE
    dep.REFERENCED_OBJECT_TYPE IN ('TABLE',
    'VIEW')
    AND dep.OBJECT_SCHEMA IN ('STAGING',
    'DATAVAULT_STAGING',
    'DATAVAULT',
    'BUSINESSOBJECTS_PITS',
    'BUSINESSOBJECTS',
    'BUSINESS_RULES',
    'ACCESSLAYER',
    'ACCESS_ERRORMART')
    AND dep.OBJECT_TYPE = 'VIEW';

	
CREATE OR REPLACE LUA SCRIPT DVB_CORE."S_TOUCH_VIEWS" (ARRAY schema_list) RETURNS ROWCOUNT AS
if #schema_list==0 then
		exit()
	end
	local view_list_query = [[SELECT VIEW_SCHEMA, VIEW_NAME FROM EXA_DBA_VIEWS WHERE VIEW_SCHEMA IN (']]..schema_list[1]..[[']]

	for element_num = 2, #schema_list do
		view_list_query = view_list_query..[[,']]..schema_list[element_num]..[[']]
	end
	view_list_query = view_list_query..[[)
	MINUS 
	SELECT DISTINCT OBJECT_SCHEMA AS VIEW_SCHEMA, OBJECT_NAME AS VIEW_NAME FROM EXA_DBA_DEPENDENCIES 
	WHERE REFERENCE_TYPE IS NOT NULL AND REFERENCED_OBJECT_TYPE IS NOT NULL AND OBJECT_SCHEMA IN (']]..schema_list[1]..[[']]

	for element_num = 2, #schema_list do
		view_list_query = view_list_query..[[,']]..schema_list[element_num]..[[']]
	end
	view_list_query = view_list_query..[[)]]

  local view_res = query(view_list_query)
	
	output('touching '..#view_res..' views...')

	for row_num = 1, #view_res do
		output(join('.', quote(view_res[row_num][1]), quote(view_res[row_num][2])))
		pquery([[DESC ::schema.::view]], {schema=quote(view_res[row_num][1]), view=quote(view_res[row_num][2])})
  end

/

COMMIT;
