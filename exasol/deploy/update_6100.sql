-- Deploy dvb:update_6100 to exasol

set define off;
set escape off;
ALTER TABLE DVB_CONFIG.JOB_SCHEDULES MODIFY COLUMN WHERE_CLAUSE_PARAMETERS VARCHAR2(2000000 BYTE);
ALTER TABLE DVB_LOG.JOB_LOAD_LOG MODIFY COLUMN WHERE_CLAUSE_PARAMETERS VARCHAR2(2000000 BYTE);

COMMIT;
