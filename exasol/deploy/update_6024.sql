set define off;
set escape off;
CREATE OR REPLACE VIEW "DVB_CORE"."TRACKING_SATELLITES" AS
SELECT
    td.table_nq_id,
    td.tracking_satellite_id,
    td.tracking_satellite_name,
    td.tracked_object_id,
    JSON_VALUE(so.OBJECT_COMMENT,
    '$.name') AS tracked_object_name,
    td.is_tracking_a_link,
    td.is_tracking_a_satellite,
    td.is_delta_load_satellite,
    td.is_full_load_satellite,
    rtl.last_full_load_time,
    td.system_id,
    s.system_name,
    s.system_color,
    s.system_comment,
    td.staging_table_id,
    td.tracking_satellite_subject_area_name,
    td.tracking_satellite_comment
FROM
    (
    SELECT
        o.OBJECT_NAME AS table_nq_id,
        LEFT(o.OBJECT_NAME,
        LEN(o.OBJECT_NAME)- 2) AS tracking_satellite_id,
        JSON_VALUE(o.OBJECT_COMMENT,
        '$.name') AS tracking_satellite_name,
        CASE WHEN LEFT(o.OBJECT_NAME,
        2) = 'S_' THEN 'H_' || dvb_core.f_string_between(o.OBJECT_NAME,
        'S_',
        '_S_')
        -- !
        ELSE 'L_' || dvb_core.f_string_between(o.OBJECT_NAME,
        'LS_',
        '_S_')
        -- !
END AS tracked_object_id,
    o.OBJECT_NAME LIKE 'LS\_%' ESCAPE '\' is_tracking_a_link,
    o.OBJECT_NAME LIKE 'S\_%' ESCAPE '\' AS is_tracking_a_satellite,
    o.OBJECT_NAME LIKE '%\_W\_TRKD\_H' ESCAPE '\' AS is_delta_load_satellite,
    o.OBJECT_NAME LIKE '%\_W\_TRKF\_H' ESCAPE '\' AS is_full_load_satellite,
    NULLIF( dvb_core.f_string_between(o.OBJECT_NAME,
    '_S_',
    '_R_'),
    '' ) AS system_id,
    'STAGING.' || dvb_core.f_string_between(o.OBJECT_NAME,
    '_S_',
    '_W_') AS staging_table_id,
    JSON_VALUE(o.OBJECT_COMMENT,
    '$.subject_area') AS tracking_satellite_subject_area_name,
    JSON_VALUE(o.OBJECT_COMMENT,
    '$.comment') AS tracking_satellite_comment,
	o.ROOT_NAME AS schema_id
FROM
    SYS.EXA_ALL_OBJECTS o
WHERE
    o.ROOT_NAME = 'DATAVAULT'
    AND( o.OBJECT_NAME LIKE 'S\_%\_W\_TRKF\_H' ESCAPE '\'
    OR o.OBJECT_NAME LIKE 'S\_%\_W\_TRKD\_H' ESCAPE '\'
    OR o.OBJECT_NAME LIKE 'LS\_%\_W\_TRKF\_H' ESCAPE '\'
    OR o.OBJECT_NAME LIKE 'LS\_%\_W\_TRKD\_H' ESCAPE '\' )
) td
LEFT JOIN dvb_core.systems s ON
    (s.system_id = td.system_id)
LEFT JOIN SYS.EXA_ALL_OBJECTS so ON
    (so.OBJECT_NAME = td.tracked_object_id AND so.ROOT_NAME = td.schema_id)
LEFT JOIN DATAVAULT."_DVB_RUNTIME_LOAD_DATA" rtl ON
    (rtl.object_id = 'DATAVAULT.' || td.tracking_satellite_id);

COMMIT;
