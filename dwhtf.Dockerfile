FROM debian:buster-slim as snowsql
RUN apt-get -qq update >/dev/null && apt -qq upgrade -y >/dev/null && apt -qq install --no-install-recommends -y curl ca-certificates unzip >/dev/null && apt clean >/dev/null 2>&1 && rm -rf /var/lib/apt/lists/* > /dev/null
RUN curl -O https://sfc-repo.snowflakecomputing.com/snowsql/bootstrap/1.2/linux_x86_64/snowsql-1.2.10-linux_x86_64.bash >/dev/null
RUN chmod u+x snow* && SNOWSQL_DEST=/bin SNOWSQL_LOGIN_SHELL=~/.profile bash snowsql-1.2.10-linux_x86_64.bash >/dev/null

FROM amazon/aws-cli:2.1.3 as installaws

FROM alpine/terragrunt:0.13.5 as tf

FROM python:3.8-slim-buster
RUN apt-get -qq update >/dev/null && apt -qq upgrade -y >/dev/null && apt -qq install -y python3-dev gcc build-essential ca-certificates libkrb5-dev libxml2-dev libxslt1-dev zlib1g-dev >/dev/null && rm -rf /var/lib/apt/lists/* > /dev/null
COPY --from=tf /bin/terraform /bin/terraform
COPY --from=tf /usr/local/bin/terragrunt /bin/terragrunt
COPY --from=snowsql /bin/snowsql /bin/snowsql
COPY --from=installaws /usr/local/aws-cli/ /usr/local/aws-cli/
COPY --from=installaws /usr/local/bin/ /usr/local/bin/
RUN pip3 -q install aws-adfs==1.24.5 >/dev/null
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
RUN /bin/snowsql >/dev/null 2>&1
RUN sed -i "/log_file =/c\log_file = ~/.snowsql/log/log" ~/.snowsql/config

ENV TF_PLUGIN_CACHE_DIR=/root/.terraform.d/plugin-cache
RUN mkdir -p $TF_PLUGIN_CACHE_DIR
COPY /docker/init.tf /work/
WORKDIR /work
RUN terraform init > /dev/null
ENTRYPOINT [ "/bin/bash" ]
