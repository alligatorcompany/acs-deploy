remote_state {
  backend = "s3"
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite_terragrunt"
  }
  config = {
    bucket = "gvl-terraform-snowflake-remote-state-storage-s3"
    encrypt = true
    key = "${path_relative_to_include()}/snowflake"
    region = "eu-central-1"
    dynamodb_table = "gvl-terraform-snowflake-locking"
  }
}

terraform {
  extra_arguments "conditional_vars" {
    commands = [
      "apply",
      "plan",
      "import",
      "push",
      "refresh"
    ]
  }
}

generate "versions" {
  path      = "versions_override.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
    terraform {
      required_providers {
        snowflake = {
          source  = "chanzuckerberg/snowflake"
          version = "0.23.2"
        }
      }
    }
EOF
}

generate "provider" {
  path = "provider_snowflake.tf"
  if_exists = "overwrite_terragrunt"
  contents = <<EOF
provider "snowflake" {
  account = var.snowflake_account
  region  = "eu-central-1"
  role = var.snowflake_role
}
EOF  
}

generate "common" {
  path = "common.tf"
  if_exists = "overwrite_terragrunt"
  contents = <<EOF
variable "snowflake_account" {
    description = "Snowflake account"
    type = string
}
variable "snowflake_role" {
    description = "Snowflake role  for login"
    type = string
}
EOF
}
