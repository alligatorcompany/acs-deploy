module "snowflake_global" {
    source = "../modules/base"
    authenticator_password = "R3ZsSW5zdGFsbGF0aW9uMTIzIQ=="

    snowflake_developers = [ "DUMMY" ]

    snowflake_dbas = [ "TFDEPLOYER", "TGLUNDE" ]

    snowflake_admins = [ "DBA" ]

    snowflake_users = {
        "dummy" = {
            email = "dummy@exammple.com",
            first_name = "dummy",
            last_name = "dummy",
            login_name = "DUMMY",
            rsa_public_key = ""
        }
    }
}
