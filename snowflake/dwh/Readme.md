## Neuen Entwickler anlegen
1. Entwickler sollte einen RSA public key nach der Anleitung https://community.snowflake.com/s/article/SQL-execution-error-New-public-key-rejected-by-current-policy-Reason-Invalid-public-key anlegen
2. Eintrag in iac/dwh/global/main.tf unter `snowflake_users` einfügen:
```
        "vorname_nachname" = {
            email = "",
            first_name = "vorname",
            last_name = "nachname",
            login_name = "VORNAMENACHNAME",
            rsa_public_key = ""
        },
``` 
3. Eintrag mit login_name (case sensitive) in `snowflake_developers` einfügen: 
``` 
snowflake_developers = [ "CLAUDIAMAIHOLD","MARTINJAHN","ANDREASMUELLER","TOBIASMAY","MARCOCAESAR","TORSTENGLUNDE" ]
```
4. Im ordner iac/dwh den Befehl `dwhtf terragrunt apply-all` ausführen
5. Im Regelfall bekommt der Entwickler noch eine Umgebung (siehe nächster Punkt)
## Neue Umgebung für Entwickler anlegen
1. Im Ordner iac/dwh/dev/ einen ordner mit dem `Environmentname` anlegen
2. Dateien main.tf und terragrunt.hcl aus einem anderen dev-Ordner in den neuen Ordner kopieren
3. main.tf folgendermaßen anpassen
module "snowflake_devdb" {
    source = "../../modules/devdb"
    db_name = "DEV_`Environmentname`"
}
4. Im ordner iac/dwh den Befehl `dwhtf terragrunt apply-all` ausführen

## Runtime:
- login aws account der GVL
AWS-ADFS-login: https://confluence.gvl.de/display/IT/AWS+und+terraform#AWSundterraform-AWSCLI
- aws.config als template für defaults im Benutzerhome Verzeichnis unter .aws (~/.aws/ oder C:/Benutzer/<user>/.aws)
```
[default]
output = json
region = eu-central-1
adfs_config.ssl_verification = True
adfs_config.role_arn = arn:aws:iam::716101048629:role/GVL_AD_adfs-snowflake
adfs_config.adfs_host = adfs.gvl.de
adfs_config.session_duration = 28800
```

Folgende Vorbereitungen müssen durchgeführt werden:
1. Pfad erweitern: SET PATH=%PATH;<git-dir>/bin bzw. EXPORT PATH=$PATH:<git-dir>/bin

2. template im bin folder copieren und chmod ausführen
cp dwhtf.template dwhtf && chmod u+x dwhtf

3. Anmeldung Snowflake über RSA-key
- anlage RSA key - speichern in %USERPROFILE%/.ssh/ in Windows oder in $HOME unter linux
- im bin/dwhtf(.cmd) werden folgende parameter festgelegt
SET SNOWFLAKE_ACCOUNT=sh26514
SET SNOWFLAKE_ROLE=accountadmin
SET SNOWFLAKE_USER=tglunde
SET SNOWFLAKE_PRIVATE_KEY=id_rsa_gvl_sflake
SET PROJECT_DIR=D:\project\dwh_2020\iac\dwh

4. Anmeldung für aws, zur Ausführung von iac-befehlen
- ggf. muss das ~/.aws Verzeichnis angelegt werden (C:\Users\<username>\.aws für Windows)
dwhtf aws-adfs login

5. terraform konfigurationen ausführen
- in das verzeichnis <git-projekt>/iac/dwh wechseln und folgende befehle ausführen
- Installation validieren:
`dwhtf terragrunt validate-all`
- Plan von Terraform anschauen:
`dwhtf terragrunt plan-all`
- Terraform apply ausführen:
`dwhtf terragrunt apply-all`

## Weiterführende links
https://github.com/wcpolicarpio/terraform-up-and-running-code

https://terragrunt.gruntwork.io/docs/#getting-started

https://registry.terraform.io/providers/chanzuckerberg/snowflake/latest/docs

## manual build:
cd /docker/
docker build -f dwhtf.Dockerfile -t dwhtf:0.13.5 .
wenn das lokal gebildete image verwendet werden soll, dann muss im dwhtf(.cmd) der image tag angepasst werden!
