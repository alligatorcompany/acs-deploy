terraform {
    required_providers {
        snowflake = {
            source  = "chanzuckerberg/snowflake"
            version = "0.23.2"
        }
    }
}

locals {
  dvb_schema = toset(["ACCESSLAYER","ACCESS_ERRORMART","BUSINESSOBJECTS","BUSINESS_RULES",
               "DATAVAULT","DATAVAULT_STAGING","DVB_CONFIG","DVB_CORE","STAGING","DVB_LOG"])
}


resource "snowflake_database" "dev_db" {
    name = var.db_name
    comment = "Development Database"
}


resource "snowflake_database_grant" "dev_db_grant" {
    depends_on = [
        snowflake_database.dev_db
    ]
    database_name = var.db_name 
    privilege = "USAGE"
    roles = [ "DATAVAULTBUILDER", "DEVELOPER" , "ELTJOB" ]
    with_grant_option = true
}

resource "snowflake_database_grant" "dev_db_grant_create" {
    depends_on = [
        snowflake_database.dev_db
    ]
    database_name = var.db_name 
    privilege = "CREATE SCHEMA"
    roles = [ "DATAVAULTBUILDER", "DEVELOPER" , "ELTJOB" ]
    with_grant_option = true
}

resource "snowflake_database_grant" "dev_db_grant_monitor" {
    depends_on = [
        snowflake_database.dev_db
    ]
    database_name = var.db_name 
    privilege = "MONITOR"
    roles = [ "DEVELOPER" , "ELTJOB" ]
    with_grant_option = true
}

resource "snowflake_table_grant" "dev_table_grant_select" {
    depends_on = [
        snowflake_database.dev_db, snowflake_database_grant.dev_db_grant
    ]
    database_name = var.db_name
    privilege = "SELECT"
    with_grant_option = false
    roles = [ "DEVELOPER" ]
    on_future = true
}
resource "snowflake_table_grant" "dev_table_grant_ref" {
    depends_on = [
        snowflake_database.dev_db, snowflake_database_grant.dev_db_grant
    ]
    database_name = var.db_name
    privilege = "REFERENCES"
    with_grant_option = false
    roles = [ "DEVELOPER" ]
    on_future = true
}

resource "snowflake_view_grant" "dev_view_grant_select" {
    depends_on = [
        snowflake_database.dev_db, snowflake_database_grant.dev_db_grant
    ]
    database_name = var.db_name
    privilege = "SELECT"
    with_grant_option = false
    roles = [ "DEVELOPER" ]
    on_future = true
}

resource "snowflake_schema_grant" "dev_schema_grant_usage" {
    depends_on = [
        snowflake_database.dev_db, snowflake_database_grant.dev_db_grant
    ]
    database_name = var.db_name
    privilege = "USAGE"
    with_grant_option = false
    roles = [ "DEVELOPER" ]
    on_future = true
}
resource "snowflake_schema_grant" "dev_schema_grant_modify" {
    depends_on = [
        snowflake_database.dev_db, snowflake_database_grant.dev_db_grant
    ]
    database_name = var.db_name
    privilege = "MODIFY"
    with_grant_option = false
    roles = [ "DEVELOPER" ]
    on_future = true
}
resource "snowflake_schema_grant" "dev_schema_grant_monitor" {
    depends_on = [
        snowflake_database.dev_db, snowflake_database_grant.dev_db_grant
    ]
    database_name = var.db_name
    privilege = "MONITOR"
    with_grant_option = false
    roles = [ "DEVELOPER" ]
    on_future = true
}
resource "snowflake_schema_grant" "dev_schema_grant_table" {
    depends_on = [
        snowflake_database.dev_db, snowflake_database_grant.dev_db_grant
    ]
    database_name = var.db_name
    privilege = "CREATE TABLE"
    with_grant_option = false
    roles = [ "DEVELOPER" ]
    on_future = true
}
resource "snowflake_schema_grant" "dev_schema_grant_view" {
    depends_on = [
        snowflake_database.dev_db, snowflake_database_grant.dev_db_grant
    ]
    database_name = var.db_name
    privilege = "CREATE VIEW"
    with_grant_option = false
    roles = [ "DEVELOPER" ]
    on_future = true
}
resource "snowflake_schema_grant" "dev_schema_grant_stage" {
    depends_on = [
        snowflake_database.dev_db, snowflake_database_grant.dev_db_grant
    ]
    database_name = var.db_name
    privilege = "CREATE STAGE"
    with_grant_option = false
    roles = [ "DEVELOPER" ]
    on_future = true
}
resource "snowflake_schema_grant" "dev_schema_grant_fileformat" {
    depends_on = [
        snowflake_database.dev_db, snowflake_database_grant.dev_db_grant
    ]
    database_name = var.db_name
    privilege = "CREATE FILE FORMAT"
    with_grant_option = false
    roles = [ "DEVELOPER" ]
    on_future = true
}
resource "snowflake_schema_grant" "dev_schema_grant_ext" {
    depends_on = [
        snowflake_database.dev_db, snowflake_database_grant.dev_db_grant
    ]
    database_name = var.db_name
    privilege = "CREATE EXTERNAL TABLE"
    with_grant_option = false
    roles = [ "DEVELOPER" ]
    on_future = true
}
resource "snowflake_schema_grant" "dev_schema_grant_mat" {
    depends_on = [
        snowflake_database.dev_db, snowflake_database_grant.dev_db_grant
    ]
    database_name = var.db_name
    privilege = "CREATE MATERIALIZED VIEW"
    with_grant_option = false
    roles = [ "DEVELOPER" ]
    on_future = true
}

resource "snowflake_schema" "dev_schema_dvb" {
    for_each = local.dvb_schema
    depends_on = [
        snowflake_database.dev_db
    ]
    name = each.value
    database = var.db_name
    is_transient = true
}
resource "snowflake_table_grant" "dev_table_grant_dvb_select" {
    for_each = local.dvb_schema
    depends_on = [
        snowflake_database.dev_db, snowflake_schema.dev_schema_dvb
    ]
    database_name = var.db_name
    schema_name = each.value
    privilege = "SELECT"
    with_grant_option = false
    roles = [ "DATAVAULTBUILDER" ]
    on_future = true
}
resource "snowflake_table_grant" "dev_table_grant_dvb_insert" {
    for_each = local.dvb_schema
    depends_on = [
        snowflake_database.dev_db, snowflake_schema.dev_schema_dvb
    ]
    database_name = var.db_name
    schema_name = each.value
    privilege = "INSERT"
    with_grant_option = false
    roles = [ "DATAVAULTBUILDER" ]
    on_future = true
}
resource "snowflake_table_grant" "dev_table_grant_dvb_update" {
    for_each = local.dvb_schema
    depends_on = [
        snowflake_database.dev_db, snowflake_schema.dev_schema_dvb
    ]
    database_name = var.db_name
    schema_name = each.value
    privilege = "UPDATE"
    with_grant_option = false
    roles = [ "DATAVAULTBUILDER" ]
    on_future = true
}
resource "snowflake_table_grant" "dev_table_grant_dvb_delete" {
    for_each = local.dvb_schema
    depends_on = [
        snowflake_database.dev_db, snowflake_schema.dev_schema_dvb
    ]
    database_name = var.db_name
    schema_name = each.value
    privilege = "DELETE"
    with_grant_option = false
    roles = [ "DATAVAULTBUILDER" ]
    on_future = true
}
resource "snowflake_table_grant" "dev_table_grant_dvb_truncate" {
    for_each = local.dvb_schema
    depends_on = [
        snowflake_database.dev_db, snowflake_schema.dev_schema_dvb
    ]
    database_name = var.db_name
    schema_name = each.value
    privilege = "TRUNCATE"
    with_grant_option = false
    roles = [ "DATAVAULTBUILDER" ]
    on_future = true
}
resource "snowflake_view_grant" "dev_view_grant_dvb_select" {
    for_each = local.dvb_schema
    depends_on = [
        snowflake_database.dev_db, snowflake_schema.dev_schema_dvb
    ]
    database_name = var.db_name
    schema_name = each.value
    privilege = "SELECT"
    with_grant_option = false
    roles = [ "DATAVAULTBUILDER" ]
    on_future = true
}
resource "snowflake_schema_grant" "dev_schema_grant_dvb_usage" {
    for_each = local.dvb_schema
    depends_on = [
        snowflake_database.dev_db, snowflake_schema.dev_schema_dvb
    ]
    database_name = var.db_name
    schema_name = each.value
    privilege = "OWNERSHIP"
    with_grant_option = false
    roles = [ "DATAVAULTBUILDER" ]
}
