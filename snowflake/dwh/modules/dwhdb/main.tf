terraform {
    required_providers {
        snowflake = {
            source  = "chanzuckerberg/snowflake"
            version = "0.23.2"
        }
    }
}

locals {
  dvb_schema = toset(["ACCESSLAYER","ACCESS_ERRORMART","BUSINESSOBJECTS","BUSINESS_RULES",
               "DATAVAULT","DATAVAULT_STAGING","DVB_CONFIG","DVB_CORE","STAGING","DVB_LOG"])
  app_schema = toset(["DBT_APP_SENDEMINUTEN"])
  psa_schema = "YYYPSA"
  database = "${var.env_prefix}_${var.db_name}"
  whe_load = "${local.database}_WH_LOAD"
  whe_read = "${local.database}_WH_READ"
  dvb_role = "${local.database}_DATAVAULTBUILDER"
  elt_role = "${local.database}_ELTJOB"
  elt_user = "${local.database}_ELTUSER"
  tst_role = "${local.database}_TESTER"
  fre_role = "${local.database}_BIUSER"
  fre_user = "${local.database}_READER"
  roles = toset([ local.dvb_role, local.elt_role, local.tst_role, local.fre_role ])
  read_roles = toset([ local.dvb_role, local.elt_role, local.tst_role ])
  priv_roles = toset([ local.elt_role ])
  testers = var.snowflake_testers
}

resource "snowflake_role" "dwh_role" {
    for_each = local.roles
    name = each.value
    comment = "dwh role ${each.value}"
}

resource "snowflake_warehouse" "dwh_wh_load" {
    auto_suspend = 60
    auto_resume = true
    comment = "Warehouse loading"
    initially_suspended = true
    name = local.whe_load
    warehouse_size = "Large"
    min_cluster_count = 1
    max_cluster_count = 5
}
resource "snowflake_warehouse_grant" "dwh_wh_load_grant" {
    depends_on = [
        snowflake_role.dwh_role, snowflake_warehouse.dwh_wh_load, snowflake_role.dwh_role
    ]
    warehouse_name = local.whe_load
    privilege = "USAGE"
    roles = [ local.dvb_role, local.elt_role ]
    with_grant_option = false
}
resource "snowflake_warehouse" "dwh_wh_read" {
    auto_suspend = 60
    auto_resume = true
    comment = "Warehouse analytics"
    initially_suspended = true
    name = local.whe_read
    warehouse_size = "Medium"
    min_cluster_count = 1
    max_cluster_count = 5
}
resource "snowflake_warehouse_grant" "dwh_wh_read_grant" {
    depends_on = [
        snowflake_role.dwh_role, snowflake_warehouse.dwh_wh_read
    ]
    warehouse_name = local.whe_read
    privilege = "USAGE"
    roles = [ local.tst_role, local.fre_role ]
    with_grant_option = false
}

resource "snowflake_user" "dwh_eltuser" {
    depends_on = [
        snowflake_role.dwh_role, snowflake_warehouse.dwh_wh_load
    ]
    default_role = local.elt_role
    default_warehouse = local.whe_load
    disabled = false
    login_name = local.elt_user
    name = local.elt_user
}

resource "snowflake_user" "dwh_frontenduser" {
    depends_on = [
        snowflake_role.dwh_role, snowflake_warehouse.dwh_wh_read
    ]
    default_role = local.fre_role
    default_warehouse = local.whe_read
    disabled = false
    login_name = local.fre_user
    name = local.fre_user
}
resource "snowflake_role_grants" "dwh_dwhadmin_role_biuser" {
    depends_on = [
        snowflake_role.dwh_role
    ]
    role_name = local.fre_role
    roles = [ "DWHDBA" ]
    users = [ local.fre_user ]
}

resource "snowflake_schema" "dwh_schema_psa" {
    depends_on = [
        snowflake_database.dwh_db
    ]
    name = local.psa_schema
    database = local.database
    is_transient = true
}
resource "snowflake_schema_grant" "dwh_schema_grant_psaelt" {
    depends_on = [
        snowflake_role.dwh_role, snowflake_database.dwh_db, snowflake_schema.dwh_schema_psa
    ]
    database_name = local.database
    schema_name = local.psa_schema
    privilege = "USAGE"
    with_grant_option = false
    roles = [ local.elt_role, local.tst_role, local.dvb_role ]
}
resource "snowflake_schema_grant" "dwh_table_grant_psaelt" {
    depends_on = [
        snowflake_database.dwh_db, snowflake_schema.dwh_schema_app, snowflake_role.dwh_role
    ]
    database_name = local.database
    schema_name = local.psa_schema
    privilege = "CREATE TABLE"
    with_grant_option = true
    roles = [ local.elt_role ]
}
resource "snowflake_schema_grant" "dwh_view_grant_psaelt" {
    depends_on = [
        snowflake_database.dwh_db, snowflake_schema.dwh_schema_app, snowflake_role.dwh_role
    ]
    database_name = local.database
    schema_name = local.psa_schema
    privilege = "CREATE VIEW"
    with_grant_option = true
    roles = [ local.elt_role ]
}
resource "snowflake_schema_grant" "dwh_view_grant_psaelt" {
    depends_on = [
        snowflake_database.dwh_db, snowflake_schema.dwh_schema_app, snowflake_role.dwh_role
    ]
    database_name = local.database
    schema_name = local.psa_schema
    privilege = "CREATE VIEW"
    with_grant_option = false
    roles = [ local.elt_role ]
}

resource "snowflake_schema_grant" "dwh_table_grant_appschema" {
    for_each = local.app_schema
    depends_on = [
        snowflake_database.dwh_db, snowflake_schema.dwh_schema_app, snowflake_role.dwh_role
    ]
    database_name = local.database
    schema_name = each.value
    privilege = "CREATE TABLE"
    with_grant_option = false
    roles = [ local.elt_role ]
}
resource "snowflake_schema_grant" "dwh_view_grant_appschema" {
    for_each = local.app_schema
    depends_on = [
        snowflake_database.dwh_db, snowflake_schema.dwh_schema_app, snowflake_role.dwh_role
    ]
    database_name = local.database
    schema_name = each.value
    privilege = "CREATE VIEW"
    with_grant_option = false
    roles = [ local.elt_role ]
}

resource "snowflake_schema" "dwh_schema_app" {
    for_each = local.app_schema
    depends_on = [
        snowflake_database.dwh_db
    ]
    name = each.value
    database = local.database
    is_transient = true
}
resource "snowflake_schema_grant" "dwh_table_grant_appschema" {
    for_each = local.app_schema
    depends_on = [
        snowflake_database.dwh_db, snowflake_schema.dwh_schema_app, snowflake_role.dwh_role
    ]
    database_name = local.database
    schema_name = each.value
    privilege = "CREATE TABLE"
    with_grant_option = true
    roles = [ local.elt_role ]
}
resource "snowflake_schema_grant" "dwh_view_grant_appschema" {
    for_each = local.app_schema
    depends_on = [
        snowflake_database.dwh_db, snowflake_schema.dwh_schema_app, snowflake_role.dwh_role
    ]
    database_name = local.database
    schema_name = each.value
    privilege = "CREATE VIEW"
    with_grant_option = true
    roles = [ local.elt_role ]
}
resource "snowflake_schema_grant" "dwh_schema_grant_biuser" {
    for_each = local.app_schema
    depends_on = [
        snowflake_role.dwh_role, snowflake_database.dwh_db, snowflake_schema.dwh_schema_app,snowflake_database_grant.dwh_db_grant
    ]
    database_name = local.database
    schema_name = each.value
    privilege = "USAGE"
    with_grant_option = false
    roles = [ local.fre_role, local.elt_role, local.tst_role ]
}
resource "snowflake_table_grant" "dwh_table_grant_biuser" {
    for_each = local.app_schema
    depends_on = [
        snowflake_database.dwh_db, snowflake_schema.dwh_schema_app, snowflake_role.dwh_role
    ]
    database_name = local.database
    schema_name = each.value
    privilege = "SELECT"
    with_grant_option = false
    roles = [ local.fre_role, local.elt_role, local.tst_role ]
    on_future = true
}
resource "snowflake_view_grant" "dwh_view_grant_biuser" {
    for_each = local.app_schema
    depends_on = [
        snowflake_database.dwh_db, snowflake_schema.dwh_schema_app, snowflake_role.dwh_role
    ]
    database_name = local.database
    schema_name = each.value
    privilege = "SELECT"
    with_grant_option = false
    roles = [ local.fre_role, local.elt_role, local.tst_role ]
    on_future = true
}
resource "snowflake_table_grant" "dwh_insert_grant_appschema" {
    for_each = local.app_schema
    depends_on = [
        snowflake_database.dwh_db, snowflake_schema.dwh_schema_app, snowflake_role.dwh_role
    ]
    database_name = local.database
    schema_name = each.value
    privilege = "INSERT"
    with_grant_option = false
    roles = [ local.elt_role ]
    on_future = true
}
resource "snowflake_table_grant" "dwh_truncate_grant_appschema" {
    for_each = local.app_schema
    depends_on = [
        snowflake_database.dwh_db, snowflake_schema.dwh_schema_app, snowflake_role.dwh_role
    ]
    database_name = local.database
    schema_name = each.value
    privilege = "TRUNCATE"
    with_grant_option = false
    roles = [ local.elt_role ]
    on_future = true
}

resource "snowflake_role_grants" "dwh_dwhadmin_role_dvb" {
    depends_on = [
        snowflake_role.dwh_role
    ]
    role_name = local.dvb_role
    roles = [ "DWHDBA" ]
    users = [ "AUTHENTICATOR" ]
}

resource "snowflake_role_grants" "dwh_dwhtest_role_tester" {
    depends_on = [
        snowflake_role.dwh_role
    ]
    role_name = local.tst_role
    roles = [ "DWHDBA" ]
    users = local.testers
}

resource "snowflake_role_grants" "dwh_elt_role_elt" {
    depends_on = [
        snowflake_role.dwh_role, snowflake_user.dwh_eltuser
    ]
    role_name = local.elt_role
    roles = [ "DWHDBA" ]
    users = [ local.elt_user ]
}

resource "snowflake_database" "dwh_db" {
    name = local.database
    comment = var.env_name
}

resource "snowflake_database_grant" "dwh_db_grant" {
    depends_on = [
        snowflake_database.dwh_db, snowflake_role.dwh_role
    ]
    database_name = local.database
    privilege = "USAGE"
    roles = local.roles
    with_grant_option = false
}

resource "snowflake_database_grant" "dwh_db_grant_monitor" {
    depends_on = [
        snowflake_database.dwh_db, snowflake_role.dwh_role
    ]
    database_name = local.database
    privilege = "MONITOR"
    roles = local.roles
    with_grant_option = false
}

resource "snowflake_database_grant" "dwh_db_grant_create" {
    depends_on = [
        snowflake_database.dwh_db, snowflake_role.dwh_role
    ]
    database_name = local.database
    privilege = "CREATE SCHEMA"
    roles = [ local.elt_role, local.dvb_role ]
    with_grant_option = false
}

resource "snowflake_table_grant" "dwh_table_grant_select" {
    depends_on = [
        snowflake_database.dwh_db, snowflake_database_grant.dwh_db_grant, snowflake_role.dwh_role
    ]
    database_name = local.database
    privilege = "SELECT"
    with_grant_option = false
    roles = local.read_roles
    on_future = true
}
resource "snowflake_table_grant" "dwh_table_grant_ref" {
    depends_on = [
        snowflake_database.dwh_db, snowflake_database_grant.dwh_db_grant, snowflake_role.dwh_role
    ]
    database_name = local.database
    privilege = "REFERENCES"
    with_grant_option = false
    roles = local.read_roles
    on_future = true
}

resource "snowflake_view_grant" "dwh_view_grant_select" {
    depends_on = [
        snowflake_database.dwh_db, snowflake_database_grant.dwh_db_grant, snowflake_role.dwh_role
    ]
    database_name = local.database
    privilege = "SELECT"
    with_grant_option = false
    roles = local.read_roles
    on_future = true
}

resource "snowflake_schema_grant" "dwh_schema_grant" {
    depends_on = [
        snowflake_database.dwh_db, snowflake_database_grant.dwh_db_grant, snowflake_role.dwh_role
    ]
    database_name = local.database
    privilege = "USAGE"
    with_grant_option = false
    roles = [ local.tst_role, local.elt_role, local.dvb_role ]
    on_future = true
}

resource "snowflake_schema" "dwh_schema_dvb" {
    for_each = local.dvb_schema
    depends_on = [
        snowflake_database.dwh_db
    ]
    name = each.value
    database = local.database
    is_transient = true
}
resource "snowflake_table_grant" "dwh_table_grant_dvb_select" {
    for_each = local.dvb_schema
    depends_on = [
        snowflake_database.dwh_db, snowflake_schema.dwh_schema_dvb, snowflake_role.dwh_role
    ]
    database_name = local.database
    schema_name = each.value
    privilege = "SELECT"
    with_grant_option = false
    roles = [ local.dvb_role, local.tst_role, local.elt_role ]
    on_future = true
}
resource "snowflake_table_grant" "dwh_table_grant_dvb_insert" {
    for_each = local.dvb_schema
    depends_on = [
        snowflake_database.dwh_db, snowflake_schema.dwh_schema_dvb, snowflake_role.dwh_role
    ]
    database_name = local.database
    schema_name = each.value
    privilege = "INSERT"
    with_grant_option = false
    roles = [ local.dvb_role ]
    on_future = true
}
resource "snowflake_table_grant" "dwh_table_grant_dvb_update" {
    for_each = local.dvb_schema
    depends_on = [
        snowflake_database.dwh_db, snowflake_schema.dwh_schema_dvb, snowflake_role.dwh_role
    ]
    database_name = local.database
    schema_name = each.value
    privilege = "UPDATE"
    with_grant_option = false
    roles = [ local.dvb_role ]
    on_future = true
}
resource "snowflake_table_grant" "dwh_table_grant_dvb_delete" {
    for_each = local.dvb_schema
    depends_on = [
        snowflake_database.dwh_db, snowflake_schema.dwh_schema_dvb, snowflake_role.dwh_role
    ]
    database_name = local.database
    schema_name = each.value
    privilege = "DELETE"
    with_grant_option = false
    roles = [ local.dvb_role ]
    on_future = true
}
resource "snowflake_table_grant" "dwh_table_grant_dvb_truncate" {
    for_each = local.dvb_schema
    depends_on = [
        snowflake_database.dwh_db, snowflake_schema.dwh_schema_dvb, snowflake_role.dwh_role
    ]
    database_name = local.database
    schema_name = each.value
    privilege = "TRUNCATE"
    with_grant_option = false
    roles = [ local.dvb_role ]
    on_future = true
}
resource "snowflake_view_grant" "dwh_view_grant_dvb_select" {
    for_each = local.dvb_schema
    depends_on = [
        snowflake_database.dwh_db, snowflake_schema.dwh_schema_dvb, snowflake_role.dwh_role
    ]
    database_name = local.database
    schema_name = each.value
    privilege = "SELECT"
    with_grant_option = false
    roles = [ local.dvb_role, local.tst_role, local.elt_role ]
    on_future = true
}
resource "snowflake_schema_grant" "dwh_schema_grant_dvb_owner" {
    for_each = local.dvb_schema
    depends_on = [
        snowflake_database.dwh_db, snowflake_schema.dwh_schema_dvb, snowflake_role.dwh_role
    ]
    database_name = local.database
    schema_name = each.value
    privilege = "OWNERSHIP"
    with_grant_option = false
    roles = [ local.dvb_role ]
}
resource "snowflake_schema_grant" "dwh_schema_grant_dvb_select" {
    for_each = local.dvb_schema
    depends_on = [
        snowflake_database.dwh_db, snowflake_schema.dwh_schema_dvb, snowflake_role.dwh_role
    ]
    database_name = local.database
    schema_name = each.value
    privilege = "USAGE"
    with_grant_option = false
    roles = [ local.elt_role,local.tst_role,local.dvb_role ]
}
