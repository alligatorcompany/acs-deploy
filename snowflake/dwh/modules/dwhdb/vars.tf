variable "db_name" {
    description = "name of database"
    type = string
}

variable "env_name" {
    description = "environment name"
    type = string
}

variable "env_prefix" {
    description = "environment prefix"
    type = string
}

variable "snowflake_testers" {
    description = "list of login_names for developer role"
    type = list(string)
}
