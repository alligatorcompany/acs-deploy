
terraform {
    required_providers {
        snowflake = {
            source  = "chanzuckerberg/snowflake"
            version = "0.23.2"
        }
    }
}

resource "snowflake_role" "dvb_role" {
    name = "DATAVAULTBUILDER"
    comment = "central dvb role"
}
resource "snowflake_role" "dvb_role_user_readonly" {
    name = "DVB_USER_READONLY"
    comment = "central dvb role"
}
resource "snowflake_role" "dvb_role_admin" {
    name = "DVB_ADMIN"
    comment = "central dvb role"
}
resource "snowflake_role" "dvb_role_operations" {
    name = "DVB_OPERATIONS"
    comment = "central dvb role"
}
resource "snowflake_role" "dvb_role_dbadmin" {
    name = "DBADMIN"
    comment = "central dvb role"
}
resource "snowflake_role" "dvb_role_pgagent" {
    name = "PGAGENT"
    comment = "central dvb role"
}

resource "snowflake_user" "dvb_authenticator" {
    name = "AUTHENTICATOR"
    password = var.authenticator_password
    default_role = "DATAVAULTBUILDER"
}

resource "snowflake_role" "developer" {
    name = "DEVELOPER"
    comment = "developer role"
}

resource "snowflake_role_grants" "dvb_roles" {
    depends_on = [
        snowflake_role.dvb_role,snowflake_user.dvb_authenticator,snowflake_role.developer
    ]
    role_name = "DATAVAULTBUILDER"
    users = ["AUTHENTICATOR"]
    roles = [ "DEVELOPER" ]
}

resource "snowflake_role" "eltjob" {
    name = "ELTJOB"
    comment = "elt job role"
}

#missing warehouse + grant
resource "snowflake_warehouse" "warehouse_dev" {
    auto_suspend = 180
    auto_resume = true
    comment = "all things development"
    initially_suspended = true
    name = "WH_DEV"
    warehouse_size = "X-Small"
}

resource "snowflake_warehouse_grant" "warehouse_dev_grant" {
    depends_on = [
        snowflake_warehouse.warehouse_dev,snowflake_role.developer, snowflake_role.eltjob, snowflake_role.dvb_role
    ]
    warehouse_name = "WH_DEV"
    privilege = "USAGE"
    roles = [ "DEVELOPER", "ELTJOB", "DATAVAULTBUILDER" ]
}

resource "snowflake_user" "users" {
    for_each = var.snowflake_users

    default_role = "DEVELOPER"
    default_warehouse = "WH_DEV"
    disabled = false
    email = each.value.email
    first_name = each.value.first_name
    last_name = each.value.last_name
    login_name = each.value.login_name
    name = each.value.login_name
    rsa_public_key = each.value.rsa_public_key
}

resource "snowflake_role_grants" "dev_user_developer" {
    depends_on = [
        snowflake_user.users, snowflake_role.developer
    ]
    role_name = "DEVELOPER"
    roles = [ "DWHDBA" ]
    users = var.snowflake_developers
}

resource "snowflake_role_grants" "dev_user_dba" {
    depends_on = [
        snowflake_user.users, snowflake_role.developer
    ]
    role_name = "DWHDBA"
    users = var.snowflake_dbas
}

resource "snowflake_role_grants" "dev_user_admin" {
    depends_on = [
        snowflake_user.users, snowflake_role.developer
    ]
    role_name = "ACCOUNTADMIN"
    users = var.snowflake_admins
}
