variable "authenticator_password" {
    description = "dvb authenticator password"
    type = string
}

variable "snowflake_users" {
    description = "list of users to create"

    type = map(object({
        login_name      = string
        email           = string
        first_name      = string
        last_name       = string
        rsa_public_key  = string
      }))
}

variable "snowflake_developers" {
    description = "list of login_names for developer role"
    type = list(string)
}

variable "snowflake_dbas" {
    description = "list of login_names for dba role"
    type = list(string)
}

variable "snowflake_admins" {
    description = "list of login_names for admin role"
    type = list(string)
}
