module "snowflake_devdb" {
    source = "../../modules/dwhdb"
    db_name = "DWH"
    env_prefix = "PRD"
    env_name = "Production, daily load"

    snowflake_testers = []

}
