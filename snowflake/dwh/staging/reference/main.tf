module "snowflake_devdb" {
    source = "../../modules/dwhdb"
    db_name = "REFERENCE"
    env_prefix = "STG"
    env_name = "Reference environment, daily load"

    snowflake_testers = [ "TORSTENGLUNDE", "DIETERNEY", "CLAUDIAMAIHOLD", "MARTINJAHN" ]

}
