module "snowflake_devdb" {
    source = "../../modules/dwhdb"
    db_name = "TESTING"
    env_prefix = "STG"
    env_name = "Test environment, daily load"

    snowflake_testers = [ "TORSTENGLUNDE", "DIETERNEY", "CLAUDIAMAIHOLD", "MARTINJAHN" ]

}
