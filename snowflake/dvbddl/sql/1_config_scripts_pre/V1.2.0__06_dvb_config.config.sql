USE DATABASE {{ dbname }};

CREATE TABLE IF NOT EXISTS DVB_CONFIG.CONFIG (
    CONFIG_KEY text NOT NULL,
    CONFIG_VALUE text,
    CONFIG_COMMENT text,
	CONFIG_IN_DEPLOYMENT boolean,
  primary key (CONFIG_KEY)
);



MERGE INTO DVB_CONFIG.CONFIG target
USING (
	SELECT 'version_branch' AS CONFIG_KEY, 'dev' AS CONFIG_VALUE, 'dev or release build of datavaultbuilder' AS CONFIG_COMMENT, FALSE AS CONFIG_IN_DEPLOYMENT FROM DUAL
) src 
ON (target.CONFIG_KEY = src.CONFIG_KEY)
WHEN MATCHED THEN
	UPDATE SET CONFIG_VALUE = src.CONFIG_VALUE, CONFIG_COMMENT = src.CONFIG_COMMENT, CONFIG_IN_DEPLOYMENT = src.CONFIG_IN_DEPLOYMENT
WHEN NOT MATCHED THEN
	INSERT (CONFIG_KEY, CONFIG_VALUE, CONFIG_COMMENT, CONFIG_IN_DEPLOYMENT)
	VALUES  (src.CONFIG_KEY, src.CONFIG_VALUE, src.CONFIG_COMMENT, src.CONFIG_IN_DEPLOYMENT)
;


MERGE INTO DVB_CONFIG.CONFIG target
USING (SELECT 'server_name' AS CONFIG_KEY, 'Datavaultbuilder' AS CONFIG_VALUE, 'Server name' AS CONFIG_COMMENT, FALSE AS CONFIG_IN_DEPLOYMENT  FROM DUAL
		UNION ALL
		SELECT 'datetime_format' AS CONFIG_KEY, 'DD.MM.YYYY HH24:MI:SS' AS CONFIG_VALUE, 'date and time format for output in gui' AS CONFIG_COMMENT, FALSE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'date_format' AS CONFIG_KEY, 'DD.MM.YYYY' AS CONFIG_VALUE, 'date format for output in gui' AS CONFIG_COMMENT, FALSE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'staging_load_async' AS CONFIG_KEY, 'TRUE' AS CONFIG_VALUE, 'sync or async load' AS CONFIG_COMMENT, TRUE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'datavault_preload_parent' AS CONFIG_KEY, 'TRUE' AS CONFIG_VALUE, 'preload hubs with sat and link loads, links with linksats' AS CONFIG_COMMENT, TRUE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'default_loading_batch_size' AS CONFIG_KEY, '50000' AS CONFIG_VALUE, 'batch size for staging load' AS CONFIG_COMMENT, TRUE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'default_staging_load_log_progress_update_size' AS CONFIG_KEY, '10000' AS CONFIG_VALUE, 'in staging, defines after how many rows a progress update will be logged' AS CONFIG_COMMENT, TRUE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'token_timeout' AS CONFIG_KEY, '300' AS CONFIG_VALUE, 'login user management in seconds' AS CONFIG_COMMENT, FALSE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'inactivity_timeout' AS CONFIG_KEY, '7200' AS CONFIG_VALUE, 'login user management in seconds' AS CONFIG_COMMENT, FALSE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'leave_site_popup' AS CONFIG_KEY, 'TRUE' AS CONFIG_VALUE, 'show popup when leave the site or reload' AS CONFIG_COMMENT , FALSE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'polling_time_staging' AS CONFIG_KEY, '5000' AS CONFIG_VALUE, 'polling frequency for load state refresh in  milliseconds' AS CONFIG_COMMENT , FALSE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'polling_time_datavault' AS CONFIG_KEY, '5000' AS CONFIG_VALUE, 'polling frequency for load state refresh in milliseconds' AS CONFIG_COMMENT , FALSE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'datavault_key_type' AS CONFIG_KEY, 'hash_bytea' AS CONFIG_VALUE, 'key type for the datavault ''hash_bytea'', ''hash_uuid'', ''bk'''  AS CONFIG_COMMENT , TRUE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'polling_time_lineage' AS CONFIG_KEY, '1801' AS CONFIG_VALUE, 'polling frequency for load state refresh in milliseconds' AS CONFIG_COMMENT , FALSE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'data_preview_timeout' AS CONFIG_KEY, '300' AS CONFIG_VALUE, 'query timeout for data preview in seconds' AS CONFIG_COMMENT , FALSE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'support_link' AS CONFIG_KEY, 'http://datavault-builder.com/en/supportchat' AS CONFIG_VALUE, 'for link in gui' AS CONFIG_COMMENT , FALSE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'include_in_accesslayer_default' AS CONFIG_KEY, 'FALSE' AS CONFIG_VALUE, 'automatically include new business ruleset in accesslayer' AS CONFIG_COMMENT , TRUE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'use_bulk_copy_on_mssql' AS CONFIG_KEY, 'TRUE' AS CONFIG_VALUE, 'if client db is mssql, do loads as bulk loads. No function on postgres' AS CONFIG_COMMENT , TRUE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'max_parallel_loads' AS CONFIG_KEY, '5' AS CONFIG_VALUE, 'global maximum of parallely running staging + datavault loads' AS CONFIG_COMMENT , TRUE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'max_parallel_staging' AS CONFIG_KEY, '3' AS CONFIG_VALUE, 'global maximum of parallely running staging loads' AS CONFIG_COMMENT , TRUE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'run_job_sql_query_on_core' AS CONFIG_KEY, 'TRUE' AS CONFIG_VALUE, 'if true, run job triggered sql query on core, otherwise run on clientdb' AS CONFIG_COMMENT , TRUE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'transaction_link_fk_constraints' AS CONFIG_KEY, 'TRUE' AS CONFIG_VALUE, 'transaction links have foreign key constraints on multi link hash keys' AS CONFIG_COMMENT , TRUE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'environment_color' AS CONFIG_KEY, '#eeeeee' AS CONFIG_VALUE, 'sets the background color of the navigation bar - can be used to distinguish environments' AS CONFIG_COMMENT , FALSE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'environment_text_color' AS CONFIG_KEY, '#000000' AS CONFIG_VALUE, 'sets the text color of the navigation bar - can be used to distinguish environments' AS CONFIG_COMMENT , FALSE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'environment_text_color_active' AS CONFIG_KEY, '##3F51B5' AS CONFIG_VALUE, 'sets the selected text color of the navigation bar - can be used to distinguish environments' AS CONFIG_COMMENT , FALSE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'version_major' AS CONFIG_KEY, '999' AS CONFIG_VALUE, 'major version of datavaultbuilder' AS CONFIG_COMMENT , FALSE AS CONFIG_IN_DEPLOYMENT FROM DUAL  -- initial version for new db, will be set correctly by core
		UNION ALL
		SELECT 'version_minor' AS CONFIG_KEY, '999' AS CONFIG_VALUE, 'minor version of datavaultbuilder' AS CONFIG_COMMENT , FALSE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'version_revision' AS CONFIG_KEY, '999' AS CONFIG_VALUE, 'revision of datavaultbuilder' AS CONFIG_COMMENT , FALSE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'version_build' AS CONFIG_KEY, '999' AS CONFIG_VALUE, 'build of datavaultbuilder' AS CONFIG_COMMENT , FALSE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'enable_beta_features' AS CONFIG_KEY, 'FALSE' AS CONFIG_VALUE, 'displays beta features in the gui' AS CONFIG_COMMENT , FALSE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'datavault_load_sat_in_batches' AS CONFIG_KEY, 'TRUE' AS CONFIG_VALUE, 'Load Current Satellite tables in batches' AS CONFIG_COMMENT , TRUE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'datavault_load_sat_batch_size' AS CONFIG_KEY, '10000000' AS CONFIG_VALUE, 'Max batch size (approximation only, number of batches will be the next higher power of 2)' AS CONFIG_COMMENT , TRUE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'datavault_load_commit_batches' AS CONFIG_KEY, 'TRUE' AS CONFIG_VALUE, 'Commit each batch during satellite load' AS CONFIG_COMMENT , TRUE AS CONFIG_IN_DEPLOYMENT FROM DUAL
		UNION ALL
		SELECT 'transaction_link_parent_hub_constraints' AS CONFIG_KEY, 'TRUE' AS CONFIG_VALUE, 'transaction links have foreign key constraints on parent hub' AS CONFIG_COMMENT , TRUE AS CONFIG_IN_DEPLOYMENT FROM DUAL
    ) sources 
ON (target.CONFIG_KEY = sources.CONFIG_KEY)
WHEN NOT MATCHED THEN
	INSERT (CONFIG_KEY, CONFIG_VALUE, CONFIG_COMMENT, CONFIG_IN_DEPLOYMENT)
	VALUES  (sources.CONFIG_KEY, sources.CONFIG_VALUE, sources.CONFIG_COMMENT, sources.CONFIG_IN_DEPLOYMENT)