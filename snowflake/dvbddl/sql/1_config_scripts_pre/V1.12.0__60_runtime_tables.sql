USE DATABASE {{ dbname }};

CREATE TABLE IF NOT EXISTS staging."_DVB_RUNTIME_TABLE_STATUS" (
  staging_table_id varchar(256) NOT NULL,
  modified_time TIMESTAMP_TZ DEFAULT (CURRENT_TIMESTAMP),
  is_up_to_date boolean DEFAULT false,
  is_delta_load boolean DEFAULT false,
  DATA_EXTRACT_START_TIME TIMESTAMP_TZ,
  APPLIED_WHERE_CLAUSE_GENERAL_PART VARCHAR(2000000),
  APPLIED_WHERE_CLAUSE_DELTA_PART VARCHAR(2000000),
  APPLIED_WHERE_CLAUSE_DELTA_PART_TEMPLATE VARCHAR(2000000),
  CONSTRAINT pk_dvb_metadata PRIMARY KEY (staging_table_id)
);

CREATE TABLE IF NOT EXISTS datavault."_DVB_RUNTIME_LOAD_DATA" (
	object_id varchar2(256) NOT NULL,
	modified_time TIMESTAMP_TZ DEFAULT (CURRENT_TIMESTAMP),
	last_full_load_time TIMESTAMP_TZ,
	CONSTRAINT pk_dvb_metadata PRIMARY KEY (object_id)
);

CREATE TABLE IF NOT EXISTS dvb_config."_DVB_RUNTIME_DOCUMENTATION" (
	documentation_nr int NOT NULL,
	generated_with_dvb_version varchar2(256) NOT NULL,
	generation_datetime TIMESTAMP_TZ DEFAULT (CURRENT_TIMESTAMP) NOT NULL ,
	generation_duration TIME NOT NULL,
	initiating_username varchar2(256) NOT NULL, 
	content varchar2(2000000) NOT NULL,
	CONSTRAINT pk_dvb_runtime_documentation PRIMARY KEY (documentation_nr)
);

CREATE TABLE IF NOT EXISTS dvb_config."_DVB_RUNTIME_BOOKMARK" (
  bookmark_name varchar2(256) NOT NULL,
  owner_username varchar2(256) NOT NULL,
  bookmark_content varchar2(2000000) NOT NULL,
  is_shared BOOLEAN DEFAULT false NOT NULL ,
  created_with_dvb_version varchar2(256) NOT NULL,
  modified_timestamp TIMESTAMP_TZ DEFAULT (CURRENT_TIMESTAMP) NOT NULL ,
  CONSTRAINT pk_dvb_runtime_bookmark PRIMARY KEY (bookmark_name, owner_username),
  bookmark_category varchar2(2000000),
  bookmark_type varchar2(2000000)  
);