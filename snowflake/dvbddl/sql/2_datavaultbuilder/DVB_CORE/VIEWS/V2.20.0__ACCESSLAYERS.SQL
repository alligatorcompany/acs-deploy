USE DATABASE {{ dbname }};

CREATE OR REPLACE FORCE VIEW "DVB_CORE"."ACCESSLAYERS" AS 
SELECT
        a.accesslayer_id,
        dvb_core.JSON_VALUE(ext.COMMENT,'name') || CASE WHEN a.functional_suffix_name IS NULL THEN '' ELSE  ' > ' || a.functional_suffix_name END   AS accesslayer_name,
        a.functional_suffix_id,
        bl.functional_suffix_name,
        (
            'H_' || a.boid
        ) AS parent_hub_id,
        dvb_core.JSON_VALUE(ext.COMMENT,'name') AS parent_hub_name,
        a.accesslayer_comment
    FROM
        (
            (
                SELECT
                    iv.table_schema || '.' || iv.table_name AS accesslayer_id,
                    COALESCE(
                        dvb_core.f_string_between(
                            (iv.table_name),
                            '',
                            '_C_'
                        ),
                        (iv.table_name)
                    ) AS boid,
                    COALESCE(
                        dvb_core.f_string_between(
                            (iv.table_name),
                            '_C_',
                            ''
                        ),
                        ''
                    ) AS functional_suffix_id,
                    UPPER( dvb_core.f_string_between(( iv.table_name ), '_C_', '' )) AS functional_suffix_name,
                    dvb_core.JSON_VALUE(iv.table_comment,'comment') AS accesslayer_comment 
                FROM
                    (
                        SELECT
                            v.TABLE_NAME AS table_name,
                            v.TABLE_SCHEMA AS table_schema,
                            v.COMMENT AS table_comment
                        FROM INFORMATION_SCHEMA."VIEWS" v
                        WHERE v.TABLE_CATALOG = CURRENT_DATABASE() AND v.TABLE_SCHEMA = 'ACCESSLAYER'
                    ) iv
                   
            ) a
        LEFT JOIN INFORMATION_SCHEMA."TABLES" ext ON (ext.TABLE_SCHEMA || '.'  || ext.table_NAME) = ('DATAVAULT.H_' || a.boid)    
        LEFT JOIN dvb_core.x_businessobjects_distinct bl ON
            (
                (
                    (
                        a.functional_suffix_id = bl.functional_suffix_id
                    )
                    AND(
                        (
                            'H_' || a.boid
                        )= bl.start_hub_id
                    )
                )
            )
        )
        WHERE ext.TABLE_CATALOG = CURRENT_DATABASE()
        ;