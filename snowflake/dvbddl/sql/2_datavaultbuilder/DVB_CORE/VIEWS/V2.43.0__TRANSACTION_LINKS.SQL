USE DATABASE {{ dbname }};


CREATE OR REPLACE VIEW DVB_CORE.TRANSACTION_LINKS AS 
    SELECT
        TD.TABLE_NQ_ID,
        TD.TRANSACTION_LINK_ID,
        TD.BOID,
        TD.SYSTEM_ID,
        S.SYSTEM_NAME,
        S.SYSTEM_COLOR,
        S.SYSTEM_COMMENT,
        TD.FUNCTIONAL_SUFFIX_ID,
        MAX( TD.FUNCTIONAL_SUFFIX_NAME ) AS FUNCTIONAL_SUFFIX_NAME,
        MAX( TD.TRANSACTION_LINK_SUBJECT_AREA_NAME ) AS TRANSACTION_LINK_SUBJECT_AREA_NAME,
        MAX( TD.TRANSACTION_LINK_COMMENT ) AS TRANSACTION_LINK_COMMENT,
        'transaction_link' AS TRANSACTION_LINK_TYPE,
        VR.TABLE_ID AS STAGING_TABLE_ID,
        COALESCE(DVB_CORE.F_STRING_BETWEEN(TD.TRANSACTION_LINK_ID, '_R_', '_F_'), DVB_CORE.F_STRING_BETWEEN(TD.TRANSACTION_LINK_ID, '_R_', '')) AS STAGING_RESOURCE_ID,
        'H_' || TD.BOID AS PARENT_HUB_ID,
        DVB_CORE.JSON_VALUE(SO.COMMENT,'name') AS PARENT_HUB_NAME,
        DVB_CORE.JSON_VALUE(SO.COMMENT,'name') || COALESCE(' > ' || NULLIF(S.SYSTEM_NAME, ''), '') || ' > ' || COALESCE(NULLIF(MAX(TD.FUNCTIONAL_SUFFIX_NAME), ''), 'Default') AS TRANSACTION_LINK_NAME,
        TD.DATAVAULT_CATEGORY_ID
    FROM
        (
            SELECT
                O.TABLE_NAME AS TABLE_NQ_ID,
                LEFT(
                    O.TABLE_NAME,
                    LENGTH(O.TABLE_NAME)- 2
                ) AS TRANSACTION_LINK_ID,
                DVB_CORE.F_STRING_BETWEEN(
                    O.TABLE_NAME,
                    'LT_',
                    '_S_'
                ) AS BOID,
                NULLIF(
                    DVB_CORE.F_STRING_BETWEEN(
                        O.TABLE_NAME,
                        '_S_',
                        '_R_'
                    ),
                    ''
                ) AS SYSTEM_ID,
                COALESCE(
                    DVB_CORE.F_STRING_BETWEEN(
                        LEFT(
                            O.TABLE_NAME,
                            LENGTH(O.TABLE_NAME)- 2
                        ),
                        '_F_',
                        ''
                    ),
                    ''
                ) AS FUNCTIONAL_SUFFIX_ID,
                COALESCE(DVB_CORE.JSON_VALUE(O.COMMENT,'datavault_category'), 'raw_vault') AS DATAVAULT_CATEGORY_ID,
                DVB_CORE.JSON_VALUE(O.COMMENT,'name') AS FUNCTIONAL_SUFFIX_NAME,
                DVB_CORE.JSON_VALUE(O.COMMENT,'subject_area')  AS TRANSACTION_LINK_SUBJECT_AREA_NAME,
                DVB_CORE.JSON_VALUE(O.COMMENT,'comment') AS TRANSACTION_LINK_COMMENT

            FROM
                INFORMATION_SCHEMA."TABLES" O
                
            WHERE
                O.TABLE_CATALOG = CURRENT_DATABASE()
                AND O.TABLE_SCHEMA = 'DATAVAULT'
                AND (
                    O.TABLE_NAME LIKE 'LT\_%H' 
                    OR O.TABLE_NAME LIKE 'LT\_%O' 
                    OR O.TABLE_NAME LIKE 'LT\_%C' 
                )
                OR O.TABLE_NAME LIKE 'LT\_%\_P' 
        ) TD
    LEFT JOIN DVB_CORE.SYSTEMS S ON
        S.SYSTEM_ID = TD.SYSTEM_ID
    LEFT JOIN DVB_CORE.VIEW_RELATIONS VR ON
        TD.TRANSACTION_LINK_ID = VR.DEPENDENT_VIEW_NQ_ID
        AND VR.DEPENDENT_VIEW_SCHEMA_ID = 'DATAVAULT_STAGING'
    LEFT JOIN INFORMATION_SCHEMA."TABLES" SO ON
        SO.TABLE_NAME = 'H_' || TD.BOID --LEFT JOIN SYS.SCHEMAS SN
 --  ON SO.SCHEMA_ID = SN.SCHEMA_ID
 --LEFT JOIN EP
 --  ON SO.OBJECT_ID = EP.MAJOR_ID
    WHERE
        SO.TABLE_CATALOG = CURRENT_DATABASE()
        AND TD.TABLE_NQ_ID LIKE 'LT\_%\_H' 
    GROUP BY
        TD.TABLE_NQ_ID,
        TD.TRANSACTION_LINK_ID,
        TD.BOID,
        TD.SYSTEM_ID,
        S.SYSTEM_NAME,
        S.SYSTEM_COLOR,
        S.SYSTEM_COMMENT,
        TD.FUNCTIONAL_SUFFIX_ID,
        VR.TABLE_ID, 
        DVB_CORE.JSON_VALUE(SO.COMMENT,'name'),
        TD.DATAVAULT_CATEGORY_ID
 ;
