USE DATABASE {{ dbname }};

CREATE OR REPLACE FORCE VIEW DVB_CORE.SYSTEMS AS 
  SELECT sd.system_id,
         sd.system_name,
         sd.system_comment,
         sc.system_color
  FROM dvb_config.system_data sd
       LEFT JOIN dvb_config.system_colors sc ON sd.system_id = sc.system_id
  WHERE sd.system_id NOT LIKE '_dvb_%'
  UNION ALL /* dummy system for offloading hubs only */
 SELECT 'DVB_HUB'::text AS system_id,
    'Hub'::text AS system_name,
    'Dummy System for Business Objects on Hubs'::text AS system_comment,
    '#ffa500'::text AS system_color
  UNION ALL
  SELECT 'DVB_PROTOTYPE' AS system_id,
	'Prototype' AS system_name,
	'Dummy System for Business Objects on Prototypes' AS system_comment,
	'#cccccc' AS system_color;