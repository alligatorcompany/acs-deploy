USE DATABASE {{ dbname }};

CREATE OR REPLACE  VIEW "DVB_CORE"."HUBS" AS 
SELECT DISTINCT
 ao.TABLE_NAME AS hub_id,
 dvb_core.JSON_VALUE(ao."COMMENT",'name') AS  hub_name,
 dvb_core.f_string_between(ao.TABLE_NAME, 'H_', '') AS boid,
 vr.table_nq_id AS hub_id_of_alias_parent,
 dvb_core.JSON_VALUE(h.comment, 'name') AS hub_name_of_alias_parent,
 dvb_core.JSON_VALUE(ao."COMMENT", 'subject_area') AS hub_subject_area_name,
 dvb_core.JSON_VALUE(ao."COMMENT", 'comment') AS hub_comment,
CAST(CASE WHEN v.TABLE_NAME IS NULL THEN 1 ELSE 0 END AS boolean) AS hub_is_prototype
FROM INFORMATION_SCHEMA."TABLES" ao
LEFT JOIN dvb_core.view_relations vr ON vr.dependent_view_nq_id = ao.TABLE_NAME AND vr.dependent_view_schema_id = ao.TABLE_SCHEMA
LEFT JOIN INFORMATION_SCHEMA."VIEWS" v ON dvb_core.f_string_between(v.TABLE_NAME, '', '_S_') = ao.TABLE_NAME AND v.TABLE_SCHEMA = 'DATAVAULT_STAGING'
LEFT JOIN INFORMATION_SCHEMA."TABLES" h ON 'DATAVAULT.' || h.TABLE_NAME = vr.table_id
WHERE ao.TABLE_CATALOG = CURRENT_DATABASE() AND v.TABLE_CATALOG = CURRENT_DATABASE() AND h.TABLE_CATALOG = CURRENT_DATABASE() 
  AND ao.TABLE_SCHEMA  = 'DATAVAULT'  AND ao.TABLE_NAME LIKE 'H\_%'
