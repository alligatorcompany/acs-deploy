# prerequisites:
Install the Snowflake python module:

  python -m pip install --upgrade pip
  pip install --upgrade snowflake-connector-python
  
detailed instruction on:
https://docs.snowflake.net/manuals/user-guide/python-connector-install.html  

# using snowchange
snowchange -a sh26514.eu-central-1 -u authenticator -r stg_testing_datavaultbuilder -w stg_testing_wh_load -d stg_testing -c stg_testing.dvb_config.metadata --create-change-history-table --vars '{"dbname": "STG_TESTING", "dvb_role": "STG_TESTING_DATAVAULTBUILDER"}' -f sql/2_datavaultbuilder

# to export use:
snowflake_by_schema.py

# to import use:
snowflake_by_schema_install.py -d NEWDBNAME


# Findings:
Most Exasol SQL Files worked out of the box!

Snowflake knows IF EXISTS for nearly everything


# Expected problems:
TIMESTAMP_TZ or TIMESTAMP_LTZ to use => check all SQL files

60 pre script => Snowflake doesn't interval

IMPERSONATE: snowflake doesn't know impersonate

