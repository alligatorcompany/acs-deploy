#!python3
import os
import shutil
import snowflake.connector
import pathlib
import argparse
import sys


# read command line
parser=argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-d','--database', action="store", dest="database", required=True, help='DB Name to install the datavault builder objects')
parser.add_argument('-w','--warehouse', action="store", dest="warehouse", required=False, default="DEMO_WH", help='Warehouse Name to install the datavault builder objects')
parser.add_argument('-a','--account', action="store", dest="account", required=False, default="gmbh2150.eu-central-1", help='Snowflake account to install the datavault builder objects')
parser.add_argument('-u','--user', action="store", dest="user", required=True, help='User Name to install the datavault builder objects')
parser.add_argument('-p','--password', action="store", dest="password", required=True, help='User Password to install the datavault builder objects')
parser.add_argument('--delete-database', action="store_true", dest="deleteDatabase", required=False, default=False, help='Deletes the database and recreates it - use with caution')



# output help if no parameters
if len(sys.argv)==1:
    parser.print_help(sys.stderr)
    sys.exit(1)

args=parser.parse_args()

################ ahrd codeded values to be replaced by args
USER = args.user
PASSWORD = args.password
ACCOUNT = args.account
WAREHOUSE = args.warehouse
DATABASE = args.database
#SCHEMA = 'DVB_CORE'


print ("trying to connect")


ctx = snowflake.connector.connect(
  user=USER,
  password=PASSWORD,
  account=ACCOUNT,
  warehouse=WAREHOUSE,
  database=DATABASE
#  schema=SCHEMA
)

print ("connection open")

#ctx = snowflake.connector.connect(
#    user='<your_user_name>',
#    password='<your_password>',
#    account='<your_account_name>'
#    )
cs = ctx.cursor()
try:
    cs.execute("SELECT current_version()")
    one_row = cs.fetchone()
    print(one_row[0])
    if args.deleteDatabase:
      cs.execute('DROP DATABASE IF EXISTS "'+DATABASE+'";')
      one_row = cs.fetchone()
      print(one_row[0])
    # create db if not existing
    cs.execute('CREATE DATABASE IF NOT EXISTS "'+DATABASE+'";')
    one_row = cs.fetchone()
    print(one_row[0])
    # use db
    print('USE DATABASE "'+DATABASE+'";')
    cs.execute('USE DATABASE "'+DATABASE+'";')
    one_row = cs.fetchone()
    print(one_row[0])
finally:
    cs.close()

##################### presql

presqlfolder= '1_config_scripts_pre'
print(sorted(os.listdir(presqlfolder)))
sortedpresqlfolder = sorted(os.listdir(presqlfolder))

for filename in sortedpresqlfolder:
    if filename.upper().endswith(".SQL") :
         # print(os.path.join(directory, filename))
      with open(os.path.join(presqlfolder,filename), 'r', encoding='utf_8') as file:
        sqlcode = file.read()
        print (filename)
        cs = ctx.cursor()
        cs.execute('USE DATABASE "'+DATABASE+'";')
        sqlsplit = sqlcode.split(";")
        for sqlsingle in sqlsplit:
          try:
            cs.execute(sqlsingle)
            result=( cs.fetchall())
            print(result)
          finally:
            pass
        cs.close()

##################### dvb_core

mainsqlfolder= '2_datavaultbuilder'
#print(sorted(os.listdir(mainsqlfolder)))
pathlist = pathlib.Path(mainsqlfolder).glob('**/*.SQL')
sortedpresqlfolder = sorted(pathlist )
print(sortedpresqlfolder)
failedcountold = 999999999999999999

# can be improved by poping out the successful views
for lp in range(15):
  failedcount = 0
  successfulFiles = []
  for filename in sortedpresqlfolder:
        #print(sortedpresqlfolder)
        #print('-----------------------------------')
           # print(os.path.join(directory, filename))
        print ("Processing file " + str(filename))
        with filename.open('r', encoding='utf_8') as file:
          sqlcode = file.read()
          #print (filename)
          cs = ctx.cursor()
          cs.execute('USE DATABASE "'+DATABASE+'";')
          #print(sqlcode)
          sqlsplit = sqlcode.split("/*split*/")
          print('split in parts:' + str(len( sqlsplit)))
          try:
            for sqlsingle in sqlsplit:
                cs.execute(sqlsingle)
                result=( cs.fetchone())
                print(result)
            successfulFiles.append(filename)
          except Exception as error:
          	failedcount +=1
          	print(" ############## Failed to install #########:")
          	print(filename)
          	print(error)

          finally:
            pass
          cs.close()
  for filename in successfulFiles:
    sortedpresqlfolder.remove(filename)
  print('failedcount '+str(failedcount))
  print('loop '+str(lp))
  #if failedcount == 0:
  if len(sortedpresqlfolder) == 0:
    print("******************* stoping because of success **********************************")
    print("******************* stoping because of success **********************************")
    print("******************* stoping because of success **********************************")
    break
  if failedcountold == failedcount:
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! still errors but stopping as no progress")
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! still errors but stopping as no progress")
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! still errors but stopping as no progress")
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! still errors but stopping as no progress")
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! still errors but stopping as no progress")
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! still errors but stopping as no progress")
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! still errors but stopping as no progress")
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! still errors but stopping as no progress")
    break
  failedcountold = failedcount

##################### postsql

presqlfolder= '3_config_scripts_post'
print(sorted(os.listdir(presqlfolder)))
sortedpresqlfolder = sorted(os.listdir(presqlfolder))

for filename in sortedpresqlfolder:
    if filename.upper().endswith(".SQL") :
         # print(os.path.join(directory, filename))
      with open(os.path.join(presqlfolder,filename), 'r', encoding='utf_8') as file:
        sqlcode = file.read()
        print (filename)
        cs = ctx.cursor()
        cs.execute('USE DATABASE "'+DATABASE+'";')
        sqlsplit = sqlcode.split(";")
        for sqlsingle in sqlsplit:
          try:
            cs.execute(sqlsingle)
            result=( cs.fetchall())
            print(result)
          except Exception as error:
            print(error)
            print(sqlsingle)
          finally:
            pass
        cs.close()


cs = ctx.cursor()
try:
# create grant wh to role datavault builder
    cs.execute('GRANT ALL ON WAREHOUSE "'+WAREHOUSE+'" TO ROLE {{ dvb_role }};')
    one_row = cs.fetchone()
    print(one_row[0])
# create grant db to role datavault builder
    cs.execute('GRANT USAGE ON DATABASE "'+DATABASE+'" TO ROLE {{ dvb_role }};')
    one_row = cs.fetchone()
    print(one_row[0])
finally:
    cs.close()

################ close connection ###########
ctx.close()
