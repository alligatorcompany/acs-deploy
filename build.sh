#!/usr/bin/env sh
set -e
if [ -z "$1" ]
then
    TAG='latest'
else
    TAG=$1
fi

kubectl build -c . \
    --skip-tls-verify \
    --insecure \
    --build-arg EXASOL_SERVICE_HOST="exasol" \
    --build-arg EXASOL_SERVICE_PORT="8563" \
    --build-arg DBT_USER="$DBT_USER" \
    --build-arg DBT_PASS="$DBT_PASS" \
    --build-arg DVB_URL="$DVB_URL" \
    --build-arg DVB_USER="$DVB_USER" \
    --build-arg DVB_PASS="$DVB_PASS" \
    -d "alligatorcompany/acs-deploy:$TAG"
exit $?
